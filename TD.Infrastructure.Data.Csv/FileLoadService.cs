﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using TD.Application.Services;
using TD.Common.Config;

namespace TD.Infrastructure.Data.Csv
{
    public interface IFileLoadService
    {
        Task LoadVendorDataFromFileAsync();
        Task LoadCustomerDataFromFileAsync();
        Task LoadItemDataFromFileAsync();
    }

    public class FileLoadService : IFileLoadService
    {
        private GenerateConfig _generateConfig;
        private static ILogger _logger;
        private readonly IAzureService _azureService;

        public FileLoadService( GenerateConfig config, ILogger logger, IAzureService azureService)
        {
            _generateConfig = config;
            _logger = logger;
            _azureService = azureService;
        }


        public async Task LoadVendorDataFromFileAsync()
        {
            _logger.LogDebug("Load Vendor Data From File");

            var filename = _generateConfig.VendorInputFilePath;
            _logger.LogDebug($"* Filename: {filename} \r\n");
            var vendors = FileHelper.GetVendorsFromFile(filename);

            try
            {
                foreach (var item in vendors)
                {
                    await _azureService.SaveVendorAsync(item);
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"LoadDataFromFile Error.");
            }
        }

        public async Task LoadCustomerDataFromFileAsync()
        {
            _logger.LogDebug("Load Customer Data From File");

            var filename = _generateConfig.CustomerInputFilePath;
            _logger.LogDebug($"* Filename: {filename} \r\n");
            var customers = FileHelper.GetCustomersFromFile(filename);

            try
            {
                foreach (var item in customers)
                {
                    await _azureService.SaveCustomerAsync(item);
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"LoadDataFromFile Error.");
            }
        }

        public async Task LoadItemDataFromFileAsync()
        {
            _logger.LogDebug("Load Item Data From File");

            var filename = _generateConfig.ItemInputFilePath;
            _logger.LogDebug($"* Filename: {filename} \r\n");
            var items = FileHelper.GetItemsFromFile(filename);

            try
            {
                foreach (var item in items)
                {
                    await _azureService.SaveItemAsync(item);
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"LoadDataFromFile Error.");
            }
        }
    }
}
