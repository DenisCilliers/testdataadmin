﻿using System.Globalization;
using CsvHelper.Configuration;
using CsvHelper.TypeConversion;
using TD.Application.Dto;

namespace TD.Infrastructure.Data.Csv.Mapping
{
    public class VendorMap : ClassMap<VendorDto>
    {
        public VendorMap()
        {
            AutoMap(CultureInfo.InvariantCulture);

            //Map(m => m.Min).TypeConverter<Int64Converter>();
            //Map(m => m.Max).TypeConverter<Int64Converter>();
            Map(m => m.Active).TypeConverter<BooleanConverter>();
            Map(m => m.StartNumber).TypeConverter<DoubleConverter>();
            
            //TableEntityItems
            //Map(m => m.RowKey).Ignore();
           Map(m => m.Timestamp).Ignore();

        }
    }
}
