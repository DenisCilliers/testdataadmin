﻿using System.Globalization;
using CsvHelper.Configuration;
using CsvHelper.TypeConversion;
using TD.Application.Dto;

namespace TD.Infrastructure.Data.Csv.Mapping
{
    public class CustomerMap : ClassMap<CustomerDto>
    {
        public CustomerMap()
        {
            AutoMap(CultureInfo.InvariantCulture);

            //Map(m => m.Min).TypeConverter<Int64Converter>();
            //Map(m => m.Max).TypeConverter<Int64Converter>();
            Map(m => m.Active).TypeConverter<BooleanConverter>();

            //TableEntityItems
            Map(m => m.RowKey).Ignore();
            Map(m => m.Timestamp).Ignore();

        }
    }
}
