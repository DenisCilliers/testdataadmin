﻿using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using CsvHelper;
using CsvHelper.Configuration;
using TD.Application.Dto;
using TD.Infrastructure.Data.Csv.Mapping;

namespace TD.Infrastructure.Data.Csv
{
    public class FileHelper
    {
        public static IEnumerable<VendorDto> GetVendorsFromFile(string fileName)
        {
            using var reader = new StreamReader(fileName);

            var config = new CsvConfiguration(CultureInfo.InvariantCulture)
            {
                HasHeaderRecord = true,
            };

            using var csv = new CsvReader(reader, config);

            csv.Context.RegisterClassMap<VendorMap>();

            var records = csv.GetRecords<VendorDto>();
            return records.ToList();
        }

        public static IEnumerable<CustomerDto> GetCustomersFromFile(string fileName)
        {
            using var reader = new StreamReader(fileName);

            var config = new CsvConfiguration(CultureInfo.InvariantCulture)
            {
                HasHeaderRecord = true,
            };

            using var csv = new CsvReader(reader, config);

            csv.Context.RegisterClassMap<CustomerMap>();

            var records = csv.GetRecords<CustomerDto>();
            return records.ToList();
        }

        public static IEnumerable<ItemDto> GetItemsFromFile(string fileName)
        {
            using var reader = new StreamReader(fileName);

            var config = new CsvConfiguration(CultureInfo.InvariantCulture)
            {
                HasHeaderRecord = true,
            };

            using var csv = new CsvReader(reader, config);

            csv.Context.RegisterClassMap<ItemMap>();

            var records = csv.GetRecords<ItemDto>();
            return records.ToList();
        }
    }
}
