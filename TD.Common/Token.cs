﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MCR.Processor.TestDataGenerator.Model
{
    public class Token
    {
        public string CallId { get; set; }
        public int ErrorCode { get; set; }
        public int ApiVersion { get; set; }
        public int StatusCode { get; set; }
        public string StatusReason { get; set; }
        public DateTime Time { get; set; }
        public string IgnoredFields { get; set; }
        public string Id_Token { get; set; }
    }
}
