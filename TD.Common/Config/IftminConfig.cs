﻿
namespace TD.Common.Config
{
    public class IftminConfig
    {
        public bool UseThirdPartyBarcodeFile { get; set; }
        public int RecordsPerFile { get; set; }
        public string DDU_DDP { get; set; }
        //public string IfsAccount { get; set; }
     
        public int RecordsPerMessage { get; set; }

        //Item
        public string ItemDescription { get; set; }
        public double ItemValueOfItem { get; set; }
        public string ItemCurrency { get; set; }
        public string ItemTarricCode { get; set; }
        public string ItemTypeOfGoods { get; set; }
        public string ItemCountryOfOrigin { get; set; }
        

        //Vendor
        public string VendorName { get; set; }
        public string VendorEmailAddress { get; set; }
        public string VendorPhoneNumber { get; set; }
        
        public string VendorAccount { get; set; }
        public double VendorStartNumber { get; set; }
        public string VendorPostfix { get; set; }
        public string VendorPrefix { get; set; }
        public string VendorCurrency { get; set; }

        //Customer 
        public string CustomerName { get; set; }
        public string CustomerEmailAddress { get; set; }
        public string CustomerPhone { get; set; }

        //ThirdParty
        public string ThirdPartyBarcodeLength { get; set; }
        public string ThirdPartyBarcodePrefix { get; set; }



    }
}
