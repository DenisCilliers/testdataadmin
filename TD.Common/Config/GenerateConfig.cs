﻿
namespace TD.Common.Config
{
    public class GenerateConfig
    {
        public string DataPath { get; set; }
        public string RootPath { get; set; }

        public string DefaultFolderName { get; set; }
        public int TrackingNumberCount { get; set; }
        
        //Filenames
        public string TrackingNumberFilename { get; set; }
        public string VendorInputFilename { get; set; }
        public string DefaultIftminFilename { get; set; }
        public string DefaultItmattFilename { get; set; }
        
        public string CustomerInputFilename { get; set; }
        public string ItemInputFileName { get; set; }
        public string ParcelInputFileName { get; set; }
        
        //FilePaths
        public string TrackingNumberFilePath => RootPath + "\\" + DefaultFolderName + "\\" + TrackingNumberFilename;
        public string IftminFilePath => RootPath + "\\" + DefaultFolderName + "\\" + DefaultIftminFilename;
        public string ItmattFilePath => RootPath + "\\" + DefaultFolderName + "\\" + DefaultItmattFilename;

        public string VendorInputFilePath => DataPath + VendorInputFilename;
        public string CustomerInputFilePath => DataPath + CustomerInputFilename;
        public string ItemInputFilePath => DataPath + ItemInputFileName;

        public string HistoryFilePath => DataPath + "\\History.csv";

    }
}
