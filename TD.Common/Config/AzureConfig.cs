﻿namespace TD.Common.Config
{
    public class AzureConfig
    {
        public string FileStorageConnection { get; set; }
        public string FileStorageShareName { get; set; }
        public string IftminFileStorageFolderName { get; set; }
        public string ItmattFileStorageFolderName { get; set; }

        public string CustomsSimulatorUrl { get; set; }
        public string TargetEmailSettingName { get; set; }
        public string TargetEmailAddress { get; set; }
        public string AccountName { get; set; }
        public string AccountKey { get; set; }
        public string StorageUrl { get; set; }
    }
}
