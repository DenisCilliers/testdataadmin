﻿namespace TD.Common.Config
{
    public class OracleConfig
    {
        public string OracleTT { get; set; }
        public string OracleTTMyDelivery { get; set; }
    }
}
