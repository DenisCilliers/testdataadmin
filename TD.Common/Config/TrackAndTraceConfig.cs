﻿namespace TD.Common.Config
{
    public class TrackAndTraceConfig
    {
        public string ServiceUrl { get; set; }
        public bool ServiceIsHttps { get; set; }
    }
}
