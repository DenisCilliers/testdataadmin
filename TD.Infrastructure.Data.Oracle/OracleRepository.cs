﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Oracle.ManagedDataAccess.Client;
using TD.Common.Config;
using TD.Common.Extensions;
using TD.Domain.Model;

namespace TD.Infrastructure.Data.Oracle
{
    public interface IOracleRepository
    {
        Task<bool> TestConnectionAsync();
        Task<List<string>> CheckTrackingNumbersAsync(List<string> trackingNumbers);
        Task<List<ScanStatus>> GetTrackingNumberStatusAsync(List<string> trackingNumbers);

        Task<List<FileErrorStatus>> GetIftminFileErrorsAync();

        Task<List<FileErrorStatus>> GetItmattFileErrorsAync();
    }

    public class OracleRepository : IOracleRepository
    {
        private readonly string _connectionString;
        private readonly ILogger<OracleRepository> _logger;

        public OracleRepository(OracleConfig oracleConfig, ILogger<OracleRepository> logger)
        {
            _logger = logger;
            _connectionString = oracleConfig.OracleTTMyDelivery;
        }

        public async Task<bool> TestConnectionAsync()
        {
            _logger.LogInformation("*  TestConnectionAsync() ");

            try
            {
                var connection = new OracleConnection(_connectionString);
                var cmd = connection.CreateCommand();

                if (connection.State != ConnectionState.Open)
                    await connection.OpenAsync();

                connection.Close();

                return true;

            }
            catch (Exception e)
            {
                _logger.LogError(e,"TestConnection Error:");

                return false;
            }
        }

        public async Task<List<string>> CheckTrackingNumbersAsync(List<string> trackingNumbers)
        {
            _logger.LogInformation($"* Checking Track&Trace for TrackingNumbers. Count: {trackingNumbers.Count}");

            //build list for sql

            var numbers = string.Join(",", trackingNumbers
                .Select(p => "'" + p.Mid(2, 9) + "'")
                .ToArray());
            
            var prefix = string.Join(",", trackingNumbers
                .Select(p => "'" + p.Left(2) + "'")
                .Distinct()
                .ToArray());

            var checkList = new List<string>();
         
            try
            {
                var connection = new OracleConnection(_connectionString);
                var cmd = connection.CreateCommand();

                if (connection.State != ConnectionState.Open)
                    connection.Open();

                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "Select  distinct " +
                                  " pgrp_code || LPAD(TO_CHAR(dsi_id), 9, '0') || country_code as TrackingNumber" +
                                  " FROM dsi_trace" +
                                  " WHERE DSI_ID in ( " + numbers + ")" +
                                  " AND pgrp_code IN (" + prefix + ")";

                var reader = cmd.ExecuteReader();

                while (await reader.ReadAsync())
                {
                    var number = reader["trackingnumber"].ToString() ?? "0";
                    if (number != "")
                        checkList.Add(number);
                }
                

                connection.Close();

                return checkList;
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"CheckTrackingNumbers: ");
            }

            return trackingNumbers;
        }


        public async Task<List<ScanStatus>> GetTrackingNumberStatusAsync(List<string> trackingNumbers)
        {
            _logger.LogInformation($"* Checking Track&Trace Scan Status: {trackingNumbers.Count} \r\n");

            //build list for sql
            var array = string.Join(",", trackingNumbers
                .Select(p => "'" + p.Mid(2,9) + "'")
                .ToArray());

            var prefix = string.Join(",", trackingNumbers
                .Select(p => "'" + p.Left(2) + "'")
                .Distinct()
                .ToArray());

            var resultList = new List<ScanStatus>();

            try
            {

                var connection = new OracleConnection(_connectionString);
                var cmd = connection.CreateCommand();

                if (connection.State != ConnectionState.Open)
                    connection.Open();

                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "Select " +
                                  " pgrp_code || LPAD(TO_CHAR(dsi_id),9,'0') || country_code as trackingnumber," +
                                  " trtype_code as scancode," +
                                  " TO_CHAR(dtrace_date, 'YYYY-MM-DD HH24:MI:SS') as scandate" +
                                  " From dsi_trace" +
                                  " Where Trtype_code in ('35','37', '73', '13', '14', '57', '56')" +
                                  " AND DSI_ID in ( " + array + ")" +
                                  " AND pgrp_code IN (" + prefix + ")" +
                                  " ORDER BY Dtrace_date";

                var reader = cmd.ExecuteReader();

                while (await reader.ReadAsync())
                {
                    var item = new ScanStatus()
                    {
                        TrackingNumber = reader["trackingnumber"].ToString() ?? "0",
                        ScanCode = reader["scancode"].ToString() ?? "0",
                        ScanDate = DateTime.Parse(reader["scandate"].ToString() ?? DateTime.MinValue.ToString())
                    };

                    resultList.Add(item);
                }

                connection.Close();
              
            }
            catch (Exception e)
            {
                _logger.LogError(e,$"Error reading from Track&Trace");
            }

            return resultList;
        }

        public async Task<List<FileErrorStatus>> GetIftminFileErrorsAync()
        {
            _logger.LogInformation($"* Checking Iftmin File Errors. \r\n");

            var resultList = new List<FileErrorStatus>();

            try
            {
                var connection = new OracleConnection(_connectionString);
                var cmd = connection.CreateCommand();

                if (connection.State != ConnectionState.Open)
                    connection.Open();

                cmd.CommandType = CommandType.Text;

                cmd.CommandText = "select " +
                                  " created_on, message, params, system_error_message" +
                                  " from sysap.glb_error" +
                                  " where object_path like 'TTSDBA.TTS_IFTMINS3%'" +
                                  " and created_on > trunc(sysdate) - 10" +
                                  " and ERROR_TYPE != 'LOG'" +
                                  " order by created_on desc";

                var reader = cmd.ExecuteReader();

                while (await reader.ReadAsync())
                {
                    var message = reader["message"].ToString();
                    if (string.IsNullOrEmpty(message))
                    {
                        message = reader["system_error_message"].ToString();
                    }

                    var item = new FileErrorStatus()
                    {
                        Message = message,
                        Details = reader["params"].ToString(),
                        ErrorDate = DateTime.Parse(reader["created_on"].ToString() 
                                                   ?? DateTime.MinValue.ToString(CultureInfo.InvariantCulture))
                    };

                    resultList.Add(item);
                }

                connection.Close();
            }
            catch (Exception e)
            {
                _logger.LogError(e,$"Error reading Iftmin File Errors");
            }

            return resultList;
        }

        public async Task<List<FileErrorStatus>> GetItmattFileErrorsAync()
        {
            Console.WriteLine($"* Checking Itmatt File Errors. \r\n");

            var resultList = new List<FileErrorStatus>();

            try
            {
                var connection = new OracleConnection(_connectionString);
                var cmd = connection.CreateCommand();

                if (connection.State != ConnectionState.Open)
                    connection.Open();

                cmd.CommandType = CommandType.Text;

                cmd.CommandText = "select " +
                                  " created_on, message, params, system_error_message" +
                                  " from sysap.glb_error" +
                                  " where object_path like '%ITMATT%'" +
                                  " and created_on > trunc(sysdate) - 10" +
                                  " and ERROR_TYPE != 'LOG'" +
                                  " AND params LIKE '%file=%'" +
                                  " order by created_on desc";

                var reader = cmd.ExecuteReader();

                while (await reader.ReadAsync())
                {
                    var message = reader["message"].ToString();
                    if (string.IsNullOrEmpty(message))
                    {
                        message = reader["system_error_message"].ToString();
                    }

                    var item = new FileErrorStatus()
                    {
                        Message = message,
                        Details = reader["params"].ToString(),
                        ErrorDate = DateTime.Parse(reader["created_on"].ToString()
                                                   ?? DateTime.MinValue.ToString(CultureInfo.InvariantCulture))
                    };

                    resultList.Add(item);
                }

                connection.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine($"Error reading Itmatt File Errors: {e.Message}");
            }

            return resultList;
        }

        public bool CheckTrackingNumbers(string trackingNumber)
        {
            _logger.LogInformation($"* Checking Track&Trace for TrackingNumber");

            try
            {
                var connection = new OracleConnection(_connectionString);
                var cmd = connection.CreateCommand();

                if (connection.State != ConnectionState.Open)
                    connection.Open();

                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT item_number FROM ttsdba.WEB_CUSTOMS_MASTER_VW" +
                                  " WHERE product_code = 'CN'" +
                                  " AND country_code = 'IE'" +
                                  " AND item_number = '" + trackingNumber + "'";

                var dr = cmd.ExecuteReader();

                dr.Read();

                var result = dr.HasRows;

                connection.Close();

                return result;
            }
            catch (Exception e)
            {
                _logger.LogError(e,$"Error reading from Track&Trace");

            }

            return false;
        }

        public bool CheckEmailView()
        {
            _logger.LogInformation($"* Checking Track&Trace for TrackingNumber");
          

            try
            {
                var connection = new OracleConnection(_connectionString);
                var cmd = connection.CreateCommand();

                if (connection.State != ConnectionState.Open)
                    connection.Open();

                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT * FROM WEB_CNS_EMAIL_NOTIFICATION_VW where rownum <= 10";

                var dr = cmd.ExecuteReader();

                dr.Read();

                var result = dr.HasRows;

                connection.Close();

                return result;
            }
            catch (Exception e)
            {
                _logger.LogError(e,$"Error reading from Track&Trace");

            }

            return false;
        }
    }
}
