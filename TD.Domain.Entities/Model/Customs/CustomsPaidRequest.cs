﻿namespace TD.Domain.Entities.Model.Customs
{
    public class CustomsPaidRequest
    {
        public string CustomerId { get; set; }
        public string RevenueMrn { get; set; }
    }
}
