﻿using System.Collections.Generic;

namespace TD.Domain.Entities.Model.Customs
{
    public class CustomsLookupRequest
    {
        public List<string> AnPostTrackingNumbers { get; set; }
    }
}
