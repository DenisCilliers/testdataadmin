﻿namespace TD.Domain.Entities.Model.Customs
{
    public class CustomsStatus
    {
        public string TrackingNumber { get; set; }
        public string RevenueMRN { get; set; }
    }
}
