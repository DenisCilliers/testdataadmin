﻿using System;
using System.Collections.Generic;

namespace TD.Domain.Entities.Model.Customs
{
    public class PaymentLookupDetail
    {
        public string AnPostPackageTrackingNumber { get; set; }
        public string RevenueMrn { get; set; }
        public string RetailerName { get; set; }
        public bool RevenuePaymentRequired { get; set; }
        public double RevenuePaymentTotalDue { get; set; }
        public DateTime RevenuePaymentDateDue { get; set; }
        public bool Success { get; set; }
        public List<object> Errors { get; set; }
    }
}
