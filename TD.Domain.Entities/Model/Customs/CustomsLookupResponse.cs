﻿using System.Collections.Generic;

namespace TD.Domain.Entities.Model.Customs
{
    public class CustomsLookupResponse
    {
        public List<PaymentLookupDetail> PaymentLookupDetails { get; set; }
        public bool Success { get; set; }
        public List<object> Errors { get; set; }
    }
}
