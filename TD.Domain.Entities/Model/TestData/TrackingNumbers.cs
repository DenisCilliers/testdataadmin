﻿using System.Collections.Generic;

namespace TD.Domain.Entities.Model.TestData
{
    public class TrackingNumbers
    {
        public List<string> Numbers { get; set; }
    }
}
