﻿using System;
using Microsoft.Azure.Cosmos.Table;
using Newtonsoft.Json;

namespace TD.Domain.Entities.Model.TestData
{
    public class TestProfile : TableEntity
    {
        public string ProfileName { get; set; }
        public string VendorKey { get; set; }
        public string CustomerKey { get; set; }
        public string ItemKey { get; set; }
        public string FolderPath { get; set; }

        public TrackingNumbers TrackingNumbers { get; set; }

        public string TrackingNumbersJson { get; set; }

        public bool Active { get; set; }

        public void AssignRowKey()
        {
            this.RowKey = Guid.NewGuid().ToString("N");
        }
        public void AssignPartitionKey()
        {
            this.PartitionKey = "TestData";
        }

        public void BuildJson()
        {
            if (string.IsNullOrWhiteSpace(TrackingNumbersJson))
                this.TrackingNumbersJson =JsonConvert.SerializeObject(TrackingNumbers);
        }

        public void BuildTrackingNumbers()
        {
            if (!string.IsNullOrWhiteSpace(TrackingNumbersJson))
                this.TrackingNumbers = JsonConvert.DeserializeObject<TrackingNumbers>(TrackingNumbersJson);
        }

    }
}
