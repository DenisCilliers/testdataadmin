﻿using System;

namespace TD.Domain.Model
{
    public class ScanStatus
    {
        public string TrackingNumber { get; set; }
        public string ScanCode { get; set; }
        public DateTime ScanDate{ get; set; }
    }
}
