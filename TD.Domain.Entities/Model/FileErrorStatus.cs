﻿using System;

namespace TD.Domain.Model
{
    public class FileErrorStatus
    {
        public string Message { get; set; }
        public DateTime ErrorDate { get; set; }
        public string Details { get; set; }

    }
}
