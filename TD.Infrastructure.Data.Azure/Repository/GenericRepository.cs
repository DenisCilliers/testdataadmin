﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Azure.Cosmos.Table;
using Microsoft.Extensions.Logging;
using TD.Infrastructure.Data.Azure.Base;

namespace TD.Infrastructure.Data.Azure.Repository
{
    public interface IGenericRepository<TEntity>
    {
        Task<TEntity> GetAsync(string rowkey);
        Task DeleteAsync(TEntity entity);

        Task SaveAsync(TEntity entity);
        Task<IEnumerable<TEntity>> GetAllAsync();

        Task<IEnumerable<TEntity>> QueryAsync(TableQuery<TEntity> query);
    }

    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class, ITableEntity, new()
    {
        protected string TableName { get; set; }

        private readonly IAzureTableStorage _tableStorage;
        private readonly ILogger _logger;

        public GenericRepository(IAzureTableStorage tableStorage, ILogger logger)
        {
            _tableStorage = tableStorage;
            _logger = logger;
        }

        public async Task<TEntity> GetAsync(string rowkey)
        {
            var query = new TableQuery<TEntity>()
                .Where(TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.Equal, rowkey));

            //TableQuery<Vendor> query;
            var record = await _tableStorage.QueryAsync<TEntity>(TableName, query);

            return record.FirstOrDefault();
        }

        public async Task DeleteAsync(TEntity entity)
        {
            try
            {
                await _tableStorage.DeleteAsync(TableName, entity);
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"DeleteAsync Error.");
            }
        }

        public async Task SaveAsync(TEntity entity)
        {
            try
            {
                await _tableStorage.AddOrUpdateAsync(TableName, entity);
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"AddItemAsync Error.");
            }
        }

        public async Task<IEnumerable<TEntity>> GetAllAsync() 
        {
            var list = await _tableStorage.GetAllAsync<TEntity>(TableName);

            return list;
        }

        public async Task<IEnumerable<TEntity>> QueryAsync(TableQuery<TEntity> query)
        {
            var result = await _tableStorage.QueryAsync<TEntity>(TableName, query);

            return result;
        }



    }
}
