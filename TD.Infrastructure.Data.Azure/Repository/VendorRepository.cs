﻿using IFTMIN.Engine.Models;
using Microsoft.Extensions.Logging;
using TD.Infrastructure.Data.Azure.Base;

namespace TD.Infrastructure.Data.Azure.Repository
{
    public interface IVendorRepository: IGenericRepository<Vendor>
    {
    }

    public class VendorRepository: GenericRepository<Vendor>, IVendorRepository
    {
        public VendorRepository(IAzureTableStorage tableStorage, ILogger<VendorRepository> logger) : base(tableStorage, logger)
        {
            base.TableName = AzureConst.AzureVendorTableName;
        }
    }
}
