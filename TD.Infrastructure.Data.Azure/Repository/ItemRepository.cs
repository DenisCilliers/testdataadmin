﻿using IFTMIN.Engine.Models;
using Microsoft.Extensions.Logging;
using TD.Infrastructure.Data.Azure.Base;

namespace TD.Infrastructure.Data.Azure.Repository
{
    public interface IItemRepository : IGenericRepository<Item>
    {
    }

    public class ItemRepository : GenericRepository<Item>, IItemRepository
    {
        public ItemRepository(IAzureTableStorage tableStorage, ILogger<ItemRepository> logger) : base(tableStorage, logger)
        {
            base.TableName = AzureConst.AzureItemTableName;
        }
    }
}
