﻿using IFTMIN.Engine.Models;
using Microsoft.Extensions.Logging;
using TD.Infrastructure.Data.Azure.Base;

namespace TD.Infrastructure.Data.Azure.Repository
{
    public interface ICustomerRepository : IGenericRepository<Customer>
    {
    }

    public class CustomerRepository : GenericRepository<Customer>, ICustomerRepository
    {
        public CustomerRepository(IAzureTableStorage tableStorage, ILogger<CustomerRepository> logger) : base(tableStorage, logger)
        {
            base.TableName = AzureConst.AzureCustomerTableName;
        }
    }
}
