﻿using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using TD.Domain.Entities.Model.TestData;
using TD.Infrastructure.Data.Azure.Base;

namespace TD.Infrastructure.Data.Azure.Repository
{
    public interface ITestProfileRepository: IGenericRepository<TestProfile>
    {
    }

    public class TestProfileRepository : GenericRepository<TestProfile>, ITestProfileRepository
    {
        public TestProfileRepository(IAzureTableStorage tableStorage, ILogger<TestProfileRepository> logger) : base(tableStorage, logger)
        {
            base.TableName = AzureConst.AzureTestProfileTableName;
        }

      
    }
}
