﻿using System;
using System.IO;
using System.Threading.Tasks;
using Azure;
using Azure.Storage.Files.Shares;
using Microsoft.Extensions.Logging;
using TD.Common.Config;

namespace TD.Infrastructure.Data.Azure
{
    public interface IAzureFileStorage
    {
        Task LoadFileToFileStorage(string sourceFilePath, string type);
        Task CopyFileToShareAsync(string sourceFilePath, string shareName, string folderName);
    }

    public class AzureFileStorage: IAzureFileStorage
    {
        private static AzureConfig _azureConfig;
        private readonly ILogger<AzureFileStorage> _logger;

        public AzureFileStorage(AzureConfig azureConfig, ILogger<AzureFileStorage> logger)
        {
            _azureConfig = azureConfig;
            _logger = logger;
        }

        public async Task LoadFileToFileStorage(string sourceFilePath,  string type )
        {
            //BaseHelper.LogInformation($"Upload File to Azure File Storage {sourceFilePath} ");
            try
            {
                var shareName = _azureConfig.FileStorageShareName; //mailscustomsdev

                var folderName = type == "Iftmin"
                    ? _azureConfig.IftminFileStorageFolderName
                    : _azureConfig.ItmattFileStorageFolderName;
                
                //var processFile = FileHelper.GetFileNameOption(sourceFilePath);

                if (!string.IsNullOrEmpty(sourceFilePath))
                {
                    await CopyFileToShareAsync(sourceFilePath, shareName, folderName);
                    _logger.LogInformation($"* File uploaded. {sourceFilePath}");
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "LoadFileToFileStorage()");
            }
        }

        public async Task CopyFileToShareAsync(string sourceFilePath, string shareName, string folderName)
        {
            _logger.LogInformation($"CopyFileToShareAsync");
            try
            {
                //Connect to Azure
                _logger.LogInformation($"* Share Name: {shareName}");
                var share = new ShareClient(_azureConfig.FileStorageConnection, shareName);

                // Get a reference to a directory and create it
                _logger.LogInformation($"* Folder Name: {folderName}");
                var directory = share.GetDirectoryClient(folderName);

                // Get a reference to a file and upload it
                var dateStamp = DateTime.UtcNow.ToString("yyMMddHHmm");
                var source = Path.GetFileNameWithoutExtension(sourceFilePath);
                var extension = Path.GetExtension(sourceFilePath);

                var filename = $"{source}_{dateStamp}.{extension}";

                var file = directory.GetFileClient(filename);

                _logger.LogInformation($"* Filename: {filename}");

                await using var stream = File.OpenRead(sourceFilePath);

                await file.CreateAsync(stream.Length);
                await file.UploadRangeAsync(
                    new HttpRange(0, stream.Length),
                    stream);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "CopyFileToShareAsync()");
            }
        }
    }
}
