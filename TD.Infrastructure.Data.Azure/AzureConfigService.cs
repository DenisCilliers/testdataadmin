﻿using System;
using System.Threading.Tasks;
using RestSharp;
using TD.Common.Config;
using Microsoft.Extensions.Logging;

namespace TD.Infrastructure.Data.Azure
{
    public interface IAzureConfigService
    {
        AzureConfig AzureConfig { get; set; }
        Task<string> GetSimulatorEmailAsync();
        Task SetSimulatorEmailAsync(string email);
    }

    public class AzureConfigService: IAzureConfigService
    {
        public AzureConfig AzureConfig { get; set; }
        private readonly ILogger<AzureConfigService> _logger;

        private const string Authorization = "Basic JEFuUG9zdE1haWxzQ3VzdG9tc1NpbXVsYXRvckRldjpGOURuZ1BoNnRYNG1QanNkdGEwQ3hqbkczRU5ra04ybmJ4ZGk0bGlYMTV3aUNtRFFGVzV1MWpmaG5zbGc=";
        //private const string baseurl = "https://anpostmailscustomssimulatordev.scm.azurewebsites.net/api/settings/";
        //private const string settingName = "SimulatorConfiguration:TargetEmailAddress";

        public AzureConfigService(AzureConfig azureConfig, ILogger<AzureConfigService> logger)
        {
            AzureConfig = azureConfig;
            _logger = logger;
        }

        public async Task<string> GetSimulatorEmailAsync()
        {
            //Console.WriteLine($"* Get Simulator Email");

            try
            {
                var client = new RestClient(AzureConfig.CustomsSimulatorUrl + AzureConfig.TargetEmailSettingName) { Timeout = -1 };
                var request = new RestRequest(Method.GET);
                request.AddHeader("Authorization", Authorization);
                IRestResponse response = await client.ExecuteAsync(request);

                var email = response.Content.Replace($"\"", "");

                return email;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "GetSimulatorEmailAsync()");
                return "";
            }
           
        }

        public async Task SetSimulatorEmailAsync(string email)
        {
            Console.WriteLine($"\r\nSet Simulator Email");

            try
            {
                var currentEmail = await GetSimulatorEmailAsync();
                _logger.LogInformation($"* Current Email: {currentEmail }");
                _logger.LogInformation($"* Set To  Email: {email }");

                var client = new RestClient(AzureConfig.CustomsSimulatorUrl) { Timeout = -1 };
                var request = new RestRequest(Method.POST);
                request.AddHeader("Authorization", Authorization);
                request.AddHeader("Content-Type", "application/json");

                var body = @"{ """ + AzureConfig.TargetEmailSettingName + @""":""" + email + @""" }";
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                IRestResponse response = await client.ExecuteAsync(request);

                var result = response.Content;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "SetSimulatorEmailAsync()");
            }
          

        }


       
    }
}
