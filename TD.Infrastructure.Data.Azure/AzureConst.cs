﻿namespace TD.Infrastructure.Data.Azure
{
    public static class AzureConst
    {
        public const string AzureVendorTableName = "IftminVendor";
        public const string AzureCustomerTableName = "IftminCustomer";
        public const string AzureParcelTableName = "IftminParcel";
        public const string AzureItemTableName = "IftminItem";
        public const string AzureTestProfileTableName = "TestProfile";
    }
}
