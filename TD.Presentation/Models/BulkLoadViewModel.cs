﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TD.Presentation.Models
{
    public class BulkLoadViewModel
    {
        public string Filename { get; set; }
        public string Filenamepath { get; set; }
    }
}
