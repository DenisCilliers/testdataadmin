﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace TD.Presentation.Models
{
    public class GenerateViewModel
    {
        [Required]
        public string ProfileRowkey { get; set; }

        public IEnumerable<SelectListItem> ProfileNameSelectList { get; set; }
        
        [Required]
        public string VendorKey { get; set; }
        public IEnumerable<SelectListItem> VendorSelectList { get; set; }

        [Required]
        public string FolderPath { get; set; }
        [Required]
        public string FileName { get; set; }

        public string TrackingNumberFilePath { get; set; }

        public FileInfo[] FileNames { get; set; }

        [Required]
        public string Prefix { get; set; }
        [Required]
        public string Postfix { get; set; }
        [Required]
        public double StartNumber { get; set; }
        [Required]
        public int NumberCount { get; set; }
    }
}
