﻿using System.ComponentModel.DataAnnotations;

namespace TD.Presentation.Models
{
    public class ItemViewModel: BaseModel
    {
        [Required]
        public string Description { get; set; }
        [Required]
        public int Quantity { get; set; }

        public double Weight { get; set; }
        [Required]
        public string ItemTariff { get; set; }
        [Required]
        public string CountryOfOrigin { get; set; }
        [Required]
        public string Currency { get; set; }
        [Required]
        public double Price { get; set; }

        public string TypeOfGoods { get; set; }
    }
}
