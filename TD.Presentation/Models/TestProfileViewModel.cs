﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using TD.Application.Dto;

namespace TD.Presentation.Models
{
    public class TestProfileViewModel : BaseModel
    {
        public TestProfileViewModel()
        {
            BuildTrackingNumbers();
        }

        [Required] public string ProfileName { get; set; }

        public IEnumerable<SelectListItem> ProfileNameSelectList { get; set; }

        [Required] 
        public string VendorKey { get; set; }
        public string VendorName { get; set; }
        public IEnumerable<SelectListItem> VendorSelectList { get; set; }

        [Required] 
        public string CustomerKey { get; set; }
        public string CustomerName { get; set; }
        public IEnumerable<SelectListItem> CustomerSelectList { get; set; }

        [Required] public string ItemKey { get; set; }
        public string ItemName { get; set; }
        public IEnumerable<SelectListItem> ItemTypeSelectList { get; set; }

        [Required] public string FolderPath { get; set; }

        public TrackingNumbers TrackingNumbers { get; set; }

        public int Count {
            get
            {
                if (!string.IsNullOrWhiteSpace(TrackingNumbersJson))
                    TrackingNumbers = JsonConvert.DeserializeObject<TrackingNumbers>(TrackingNumbersJson);

                return TrackingNumbers != null ? TrackingNumbers.Numbers.Count : 0;
            }

        }
        
        [Required]
        public string TrackingNumbersJson { get; set; }

        private void BuildTrackingNumbers()
        {
            if (!string.IsNullOrWhiteSpace(TrackingNumbersJson))
                TrackingNumbers = JsonConvert.DeserializeObject<TrackingNumbers>(TrackingNumbersJson);
        }


    }
}
