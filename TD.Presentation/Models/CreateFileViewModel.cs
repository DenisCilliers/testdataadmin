﻿using System.IO;

namespace TD.Presentation.Models
{
    public class CreateFileViewModel
    {
        public string DefaultFolderName { get; set; }
        
        public FileInfo[] FileNames { get; set; }
    }
}
