﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Threading.Tasks;

namespace TD.Presentation.Models
{
    public class StatusReport
    {
        public string ReportName { get; set; }
        public string ProfileName { get; set; }
        public DateTime Datestamp { get; set; }

        public string MessageType { get; set; }

        public List<PivotItem> ScanPivot { get; set; }
    }
}
