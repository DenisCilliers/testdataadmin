﻿using System;


namespace TD.Presentation.Models
{
    public class FileProcessError
    {
        public DateTime DateStamp { get; set; }
        public string Message { get; set; }
        public string File { get; set; }
        public string Line { get; set; }

    }
}
