﻿using System.ComponentModel.DataAnnotations;

namespace TD.Presentation.Models
{
    public class VendorViewModel: BaseModel
    {
        [Required]
        public string IFSAccount { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Type { get; set; }
        [Required]
        public string Prefix { get; set; }
        [Required]
        public string Postfix { get; set; }

        public double StartNumber { get; set; }
        [Required]
        public string Min { get; set; }
        [Required]
        public string Max { get; set; }
        [Required]
        public string Email { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string Address3 { get; set; }

        public string Address4 { get; set; }

        public string City { get; set; }

        [Required]
        public string Country { get; set; }

        public string PostCode { get; set; }

        public string PhoneNumber { get; set; }

        public string VATIdentifier { get; set; }
        [Required]
        public string Currency { get; set; }

        public bool MyDelivery { get; set; }
    }
}
