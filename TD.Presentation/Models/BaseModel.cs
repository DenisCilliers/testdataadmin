﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TD.Presentation.Models
{
    public class BaseModel
    {
        [Required]
        public bool Active { get; set; }

        public string RowKey { get; set; }
        //public string PartitionKey { get; set; }
        public DateTime Timestamp { get; set; }
        //public string ETag { get; set; }
    }
}
