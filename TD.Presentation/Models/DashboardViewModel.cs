﻿namespace TD.Presentation.Models
{
    public class DashboardViewModel
    {

        public bool AzureConfig { get; set; }
        public bool AzureIftminDropFolder { get; set; }
        public string AzureIftminDropFolderName { get; set; }

        public bool AzureItmattDropFolder { get; set; }
        public string AzureItmattDropFolderName { get; set; }

        public bool AzureCosmosTables { get; set; }
        public string AzureCosmosTablesName { get; set; }
        
        public bool TrackAndTraceDatabase { get; set; }
        public string TrackAndTraceDatabaseName { get; set; }

        public bool TrackAndTraceWCFService { get; set; }
        public string TrackAndTraceWCFServiceName { get; set; }

        public bool CustomsDatabase { get; set; }

        public bool CustomsAPI { get; set; }

        public bool MyDeliveryAPI { get; set; }

        public bool MyDeliveryCustomsAPI { get; set; }
       
    }
}
