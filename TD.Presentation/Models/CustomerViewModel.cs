﻿using System.ComponentModel.DataAnnotations;

namespace TD.Presentation.Models
{
    public class CustomerViewModel: BaseModel
    {
        [Required]
        public string FullName { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        
        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string Address3 { get; set; }

        public string Address4 { get; set; }

        public string City { get; set; }

        public string PostalCode { get; set; }
        [Required]
        public string Country { get; set; }

        public string Phone { get; set; }

        public string Email { get; set; }
    }
}
