﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace TD.Presentation.Models
{
    public class UploadFileViewModel
    {
        [Required] public string ProfileName { get; set; }

        public IEnumerable<SelectListItem> ProfileNameSelectList { get; set; }

        public string Filename { get; set; }
        [Required]
        public string ProcessType { get; set; }
        public IEnumerable<SelectListItem> ProcessTypeSelectList { get; set; }

        [Required]
        public string DefaultFolderName { get; set; }

        public FileInfo[] FileNames { get; set; }
       
    }
}
