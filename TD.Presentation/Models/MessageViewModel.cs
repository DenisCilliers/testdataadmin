﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace TD.Presentation.Models
{
    public class MessageViewModel
    {
        public string MessageType { get; set; }
        public IEnumerable<SelectListItem> MessageTypeSelectList { get; set; }
        public string ProfileName { get; set; }
        public IEnumerable<SelectListItem> ProfileNameSelectList { get; set; }
    }
}
