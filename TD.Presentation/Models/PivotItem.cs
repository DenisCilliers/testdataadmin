﻿
using System;

namespace TD.Presentation.Models
{
    public class PivotItem
    {
        public string Id { get; set; } 
        public string No { get; set; }
        public string  ScanL5  { get; set; } 
        public string  ScanL7  { get; set; }
        public string ScanK3 { get; set; }
        
        public string  ScanD5  { get; set; } 
        public string  ScanM  { get; set; } 
        public string  ScanN  { get; set; }
        public string ScanR1 { get; set; }
        
        public string  Customs  { get; set; } 
        public string  Mrn  { get; set; } 
        public string  Paid  { get; set; }
        public DateTime DateStamp { get; set; }
    }
}
