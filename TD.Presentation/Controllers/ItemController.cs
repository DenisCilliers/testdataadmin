﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using TD.Application.Dto;
using TD.Application.Services;
using TD.Presentation.Models;

namespace TD.Presentation.Controllers
{
    public class ItemController : Controller
    {
        private readonly IAzureService _azureService;
        private readonly IMapper _mapper;

        public ItemController(IAzureService azureService, IMapper mapper)
        {
            _azureService = azureService;
            _mapper = mapper;
        }

        // GET: ItemController
        public async Task<ActionResult> Index()
        {
            var list = await _azureService.GetAllItemsAsync();

            var modelList = list.Select(item => _mapper.Map<ItemViewModel>(item))
                .ToList();

            return View(modelList);
        }

        // GET: ItemController/Details/5
        public async Task<ActionResult> Details(Guid rowkey)
        {
            var dto = await _azureService.GetItemAsync(rowkey.ToString("N"));

            var model = _mapper.Map<ItemViewModel>(dto);

            return View(model);
        }

        // GET: ItemController/Create
        public ActionResult Create()
        {
            var model = new ItemViewModel()
            {
                Active = true
            };
            model.RowKey = Guid.NewGuid().ToString("N");

            return View(model);
        }

        // POST: ItemController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([FromForm] ItemViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            try
            {
                var dto = _mapper.Map<ItemDto>(model);

                await _azureService.SaveItemAsync(dto);

                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                ViewBag.Exception = ex.Message;
                return View(model);
            }
        }

        // GET: ItemController/Edit/5
        public async Task<ActionResult> Edit(Guid rowkey)
        {
            var dto = await _azureService.GetItemAsync(rowkey.ToString("N"));

            var model = _mapper.Map<ItemViewModel>(dto);

            return View(model);
        }

        // POST: ItemController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([FromForm] ItemViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            try
            {
                var dto = _mapper.Map<ItemDto>(model);

                await _azureService.SaveItemAsync(dto);

                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                ViewBag.Exception = ex.Message;
                return View(model);
            }
        }

        // GET: ItemController/Delete/5
        public async Task<ActionResult> Delete(Guid rowkey)
        {
            var dto = await _azureService.GetItemAsync(rowkey.ToString("N"));

            var model = _mapper.Map<ItemViewModel>(dto);

            return View(model);
        }

        // POST: ItemController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete([FromForm] ItemViewModel model)
        {
            try
            {
                var dto = _mapper.Map<ItemDto>(model);

                await _azureService.DeleteItemAsync(dto);

                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                ViewBag.Exception = ex.Message;
                return View(model);
            }
        }
    }
}
