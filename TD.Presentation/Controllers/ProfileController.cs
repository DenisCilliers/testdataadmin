﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using TD.Application.Dto;
using TD.Application.Services;
using TD.Presentation.Helper;
using TD.Presentation.Models;

namespace TD.Presentation.Controllers
{
    public class ProfileController : Controller
    {
        private readonly IAzureService _azureService;
        private readonly IMapper _mapper;
        private ISelectListService _selectListService;

        public ProfileController(IAzureService azureService, IMapper mapper, ISelectListService selectListService)
        {
            _azureService = azureService;
            _mapper = mapper;
            _selectListService = selectListService;
        }

        // GET: ProfileController
        public async Task<ActionResult> Index()
        {
            //get profiles
            var list = await _azureService.GetAllTestProfilesAsync();

            var modelList = list.Select(item => _mapper.Map<TestProfileViewModel>(item)).ToList();
            
            return View(modelList);
        }

        // GET: ProfileController/Details/5
        public async Task<ActionResult> Details(Guid rowkey)
        {
            var dto = await _azureService.GetTestProfileAsync(rowkey.ToString("N"));
            
            var model = _mapper.Map<TestProfileViewModel>(dto);

            model.VendorName =  await GetVendor(model.VendorKey);
            model.CustomerName = await GetCustomer(model.CustomerKey);
            model.ItemName = await GetItem(model.ItemKey);
            
            return View(model);
        }
        
        // GET: ProfileController/Create
        public async Task<ActionResult> Create()
        {
            var model = new TestProfileViewModel
            {
                VendorSelectList = await _selectListService.GetVendorSelectList(),
                CustomerSelectList = await _selectListService.GetCustomerSelectList(),
                ItemTypeSelectList = await _selectListService.GetItemTypeSelectList(),
                Active = true,
                RowKey = Guid.NewGuid().ToString("N")
            };

            return View(model);
        }

        // POST: ProfileController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([FromForm] TestProfileViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            try
            {
                var dto = _mapper.Map<TestProfileDto>(model);
                
                await _azureService.SaveTestProfileAsync(dto);
                
                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                ViewBag.Exception = ex.Message;
                return View(model);
            }
        }

        // GET: ProfileController/Edit/5
        public async Task<ActionResult> Edit(Guid rowkey)
        {
            var dto = await _azureService.GetTestProfileAsync(rowkey.ToString("N"));

            var model = _mapper.Map<TestProfileViewModel>(dto);

            model.VendorSelectList = await _selectListService.GetVendorSelectList();
            model.CustomerSelectList = await _selectListService.GetCustomerSelectList();
            model.ItemTypeSelectList = await _selectListService.GetItemTypeSelectList();

            return View(model);
        }

        // POST: ProfileController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([FromForm] TestProfileViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            try
            {
                var dto = _mapper.Map<TestProfileDto>(model);

                await _azureService.SaveTestProfileAsync(dto);

                return RedirectToAction(nameof(Index));
            }
            catch(Exception ex)
            {
                ViewBag.Exception = ex.Message;
                return View(model);
            }
        }

        // GET: ProfileController/Delete/5
        public async Task<ActionResult> Delete(Guid rowkey)
        {
            var dto = await _azureService.GetTestProfileAsync(rowkey.ToString("N"));

            var model = _mapper.Map<TestProfileViewModel>(dto);
            model.VendorName = await GetVendor(model.VendorKey);
            model.CustomerName = await GetCustomer(model.CustomerKey);
            model.ItemName = await GetItem(model.ItemKey);


            return View(model);
        }

        // POST: ProfileController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete([FromForm] TestProfileViewModel model)
        {
            try
            {
                var dto = _mapper.Map<TestProfileDto>(model);

                await _azureService.DeleteTestProfileAsync(dto);

                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                ViewBag.Exception = ex.Message;
                return View(model);
            }
        }


        private async Task<string> GetVendor(string rowKey)
        {
            if (rowKey == null || rowKey == "0") return "*None Assigned*";
            var dto = await _azureService.GetVendorAsync(rowKey);

            return dto == null ? "*None Assigned*" : dto?.Name;
        }

        private async Task<string> GetCustomer(string rowKey)
        {
            if (rowKey == null || rowKey=="0") return "*None Assigned*";

            var dto = await _azureService.GetCustomerAsync(rowKey);

            return dto==null ? "*None Assigned*" : dto?.FullName;
        }

        private async Task<string> GetItem(string rowKey)
        {
            if (rowKey == null) return "*None Assigned*";
            if (rowKey == "0") return "Use Random Items";

            var dto = await _azureService.GetItemAsync(rowKey);

            return dto == null ? "Use Random Items" : dto?.Description;
        }

    }

   
}
