﻿using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TD.Application.Services;
using TD.Common.Config;
using TD.Common.Model;
using TD.Infrastructure.Data.Azure;
using TD.Presentation.Helper;
using TD.Presentation.Models;

namespace TD.Presentation.Controllers
{
    public class FileController : Controller
    {
        private readonly IAzureService _azureService;
        private readonly IActionService _actionService;
        private static IAzureFileStorage _fileStorage;
        private readonly ISelectListService _selectListService;
        private readonly GenerateConfig _generateConfig;

        public FileController(IAzureService azureService, 
            IActionService actionService, 
            IAzureFileStorage fileStorage, 
            ISelectListService selectListService, 
            GenerateConfig generateConfig)
        {
            _azureService = azureService;
            _actionService = actionService;
            _fileStorage = fileStorage;
            _selectListService = selectListService;
            _generateConfig = generateConfig;
        }

        public async Task<IActionResult> SetupIftminFile()
        {
            var model = new TestProfileViewModel()
            {
               ProfileNameSelectList = await _selectListService.GetTestProfileSelectList(),
               VendorSelectList = await _selectListService.GetVendorSelectList(),
               CustomerSelectList = await _selectListService.GetCustomerSelectList(),
                ItemTypeSelectList = await _selectListService.GetItemTypeSelectList()
            };

            return View(model);
        }

        public async Task<IActionResult> SetupItmattFile()
        {
            var model = new TestProfileViewModel()
            {
                ProfileNameSelectList = await _selectListService.GetTestProfileSelectList(),
                VendorSelectList = await _selectListService.GetVendorSelectList(),
                CustomerSelectList = await _selectListService.GetCustomerSelectList(),
                ItemTypeSelectList = await _selectListService.GetItemTypeSelectList()
            };

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> CreateIftminFile(TestProfileViewModel model)
        {
            if (!ModelState.IsValid)
            {
                RedirectToAction("SetupIftminFile");
            }

            var testProfile = await _azureService.GetTestProfileAsync(model.ProfileName);
            testProfile.CustomerKey = model.CustomerKey;
            testProfile.VendorKey = model.VendorKey;
            testProfile.ItemKey = model.ItemKey;
            testProfile.FolderPath = model.FolderPath;

            //Check for Random Item Generate
            var createResult = testProfile.ItemKey == "0"
                ? await _actionService.CreateIftminFileRandomItemsAsync(testProfile)
                : await _actionService.CreateIftminFilesAsync(testProfile);

            return RedirectToAction(nameof(ListIftminFile), createResult);
        }

        [HttpPost]
        public async Task<IActionResult> CreateItmattFile(TestProfileViewModel model)
        {
            if (!ModelState.IsValid)
            {
                RedirectToAction("SetupItmattFile");
            }

            var testProfile = await _azureService.GetTestProfileAsync(model.ProfileName);
            testProfile.CustomerKey = model.CustomerKey;
            testProfile.VendorKey = model.VendorKey;
            testProfile.ItemKey = model.ItemKey;
            testProfile.FolderPath = model.FolderPath;

            var createResult = testProfile.ItemKey == "0"
                ? await _actionService.CreateItmattFilesRandomItemsAsync(testProfile)
                : await _actionService.CreateItmattFilesAsync(testProfile);

            return RedirectToAction(nameof(ListIftminFile), createResult);
        }

        public IActionResult ListIftminFile(CreateFileDto dto)
        {
            var resultModel = FileHelper.GetFilesInFolder(dto, "*IFTMIN*");

            return View(resultModel);
        }
        
        public IActionResult ListItmattFile(CreateFileDto dto)
        {
            var resultModel = FileHelper.GetFilesInFolder(dto, "*003_ICS2*");

            return View(resultModel);
        }

        public async Task<JsonResult> GetProfileData(string rowkey)
        {
            if (rowkey == null) return null;

            var profile = await _azureService.GetTestProfileAsync(rowkey);
            
            var result = new JsonResult(profile);
            
            return result;
        }
        
        public async Task<IActionResult> SetupUploadFile()
        {
            var model = new UploadFileViewModel
            {
                ProfileNameSelectList = await _selectListService.GetTestProfileSelectList(),
                ProcessTypeSelectList = _selectListService.GetProcessTypeSelectList()
            };

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> SetupUploadFile(UploadFileViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var testProfile = await _azureService.GetTestProfileAsync(model.ProfileName);

            if (model.ProcessType == "Iftmin")
            {
                var iftminFile = _generateConfig.RootPath + "\\" + testProfile.FolderPath + "\\"
                                 + _generateConfig.DefaultIftminFilename.Replace("Default", testProfile.ProfileName);

                var createResult = new CreateFileDto() { DefaultFolderName = Path.GetFullPath(iftminFile) };
                
                return RedirectToAction("ListIftminFile", "File", createResult);
            }
            else
            {
                var itmatFile = _generateConfig.RootPath + "\\" + testProfile.FolderPath + "\\" + _generateConfig.DefaultItmattFilename;

                var createResult = new CreateFileDto() { DefaultFolderName = Path.GetFullPath(itmatFile) };

                return RedirectToAction("ListItmattFile", "File", createResult);
            }
            
        }
        
        [HttpPost]
        public async Task<IActionResult> UploadFile(UploadFileViewModel model)
        {
            await _fileStorage.LoadFileToFileStorage(model.Filename, model.ProcessType);

            var createResult = new CreateFileDto() { DefaultFolderName = Path.GetFullPath(model.Filename) };

            return RedirectToAction(model.ProcessType=="Iftmin" ? nameof(ListIftminFile) : nameof(ListItmattFile), new { dto = createResult });
        }
        
        public IActionResult BulkLoadVendor()
        {
            var model = new BulkLoadViewModel();

            return View(model);
        }

        [HttpPost]
        public IActionResult BulkLoadVendor(BulkLoadViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

           

            return View(model);
        }
    }
}
