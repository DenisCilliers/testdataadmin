﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using TD.Application.Dto;
using TD.Application.Services;
using TD.Presentation.Models;

namespace TD.Presentation.Controllers
{
    public class VendorController : Controller
    {
        private readonly IAzureService _azureService;
        private readonly IMapper _mapper;

        public VendorController(IAzureService azureService, IMapper mapper)
        {
            _azureService = azureService;
            _mapper = mapper;
        }

        // GET: VendorController
        public async Task<ActionResult> Index()
        {
            var list = await _azureService.GetAllVendorsAsync();

            var modelList = list.Select(item => _mapper.Map<VendorViewModel>(item))
                .ToList();

            return View(modelList);
        }

        // GET: VendorController/Details/5
        public async Task<ActionResult> Details(Guid rowkey)
        {
            var dto = await _azureService.GetVendorAsync(rowkey.ToString("N"));

            var model = _mapper.Map<VendorViewModel>(dto);

            return View(model);
        }

        // GET: VendorController/Create
        public ActionResult Create()
        {
            var model = new VendorViewModel()
            {
                Active = true
            };
            model.RowKey = Guid.NewGuid().ToString("N");

            return View(model);
        }

        // POST: VendorController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([FromForm] VendorViewModel model)
        {

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            try
            {
                var dto = _mapper.Map<VendorDto>(model);

                await _azureService.SaveVendorAsync(dto);

                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                ViewBag.Exception = ex.Message;
                return View(model);
            }
        }

        // GET: VendorController/Edit/5
        public async Task<ActionResult> Edit(Guid rowkey)
        {
            var dto = await _azureService.GetVendorAsync(rowkey.ToString("N"));

            var model = _mapper.Map<VendorViewModel>(dto);

            return View(model);
        }

        // POST: VendorController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([FromForm] VendorViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            try
            {
                var dto = _mapper.Map<VendorDto>(model);

                await _azureService.SaveVendorAsync(dto);

                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                ViewBag.Exception = ex.Message;
                return View(model);
            }
        }

        // GET: VendorController/Delete/5
        public async Task<ActionResult> Delete(Guid rowkey)
        {
            var dto = await _azureService.GetVendorAsync(rowkey.ToString("N"));

            var model = _mapper.Map<VendorViewModel>(dto);

            return View(model);
        }

        // POST: VendorController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete([FromForm] VendorViewModel model)
        {
            try
            {
                var dto = _mapper.Map<VendorDto>(model);

                await _azureService.DeleteVendorAsync(dto);

                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                ViewBag.Exception = ex.Message;
                return View(model);
            }
        }
    }
}
