﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Diagnostics;
using System.Threading.Tasks;
using TD.Application.Services;
using TD.Common.Config;
using TD.Presentation.Models;

namespace TD.Presentation.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IReportService _reportService;
        private readonly IAzureService _azureService;
        private readonly TrackAndTraceConfig _trackAndTraceConfig;
        private readonly AzureConfig _azureConfig;
        private readonly OracleConfig _oracleConfig;

        public HomeController(ILogger<HomeController> logger, 
            IReportService reportService, 
            IAzureService azureService, 
            TrackAndTraceConfig trackAndTraceConfig,
            AzureConfig azureConfig, 
            OracleConfig oracleConfig)
        {
            _logger = logger;
            _reportService = reportService;
            _azureService = azureService;
            _trackAndTraceConfig = trackAndTraceConfig;
            _azureConfig = azureConfig;
            _oracleConfig = oracleConfig;
        }

        public async Task<IActionResult> Index()
        {

            await MigrateCustomers();
            await MigrateVendors();


            var dashboard = new DashboardViewModel();

            dashboard.AzureConfig = false;

            dashboard.AzureIftminDropFolder = true;
            dashboard.AzureIftminDropFolderName = _azureConfig.IftminFileStorageFolderName;

            dashboard.AzureItmattDropFolder = true;
            dashboard.AzureItmattDropFolderName = _azureConfig.ItmattFileStorageFolderName;

            dashboard.AzureCosmosTables = await _azureService.TestConnectionAsync();
            dashboard.AzureCosmosTablesName = _azureConfig.AccountName;

            dashboard.TrackAndTraceDatabase = await _reportService.TestConnectionAsync();
            dashboard.TrackAndTraceDatabaseName = GetDataSource(_oracleConfig.OracleTTMyDelivery);

            dashboard.TrackAndTraceWCFService = true;
            dashboard.TrackAndTraceWCFServiceName = _trackAndTraceConfig.ServiceUrl;

            dashboard.CustomsDatabase = false;
            dashboard.CustomsAPI = false;

            dashboard.MyDeliveryAPI = false;
            dashboard.MyDeliveryCustomsAPI = false;
            
            return View(dashboard);
        }

        //private async Task MigrateTestProfiles()
        //{
        //    var list = await _azureService.GetAllTestProfilesAsync();

        //    foreach (var item in list)
        //    {
        //        item.FolderPath = @"TestData\\Itmatt";
        //        item.CustomerKey = "05e7865e16c4465a9867fd0209f7fcb8"; //Joe Blogs
        //        item.VendorKey = "f46311732383475ea6ec473061753cbc"; //Default Company
        //        item.ItemKey = "1b0af7f338a74854adf78b929cca7923"; //Digital Recorder

        //        await _azureService.SaveTestProfileAsync(item);
        //    }
        //}

        private async Task MigrateCustomers()
        {
            var list = await _azureService.GetAllCustomersAsync();

            foreach (var item in list)
            {
                item.Active = true;
                
                await _azureService.SaveCustomerAsync(item);
            }
        }

        private async Task MigrateVendors()
        {
            var list = await _azureService.GetAllVendorsAsync();

            foreach (var item in list)
            {
                item.Active = true;

                await _azureService.SaveVendorAsync(item);
            }
        }

        private string GetDataSource(string connString)
        {
            var len = connString.Length;
            var index = connString.IndexOf("Data Source=",0);
            var datasource = connString.Substring(index).Replace("Data Source=","");

            return datasource;
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
