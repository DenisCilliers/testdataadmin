﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using TD.Application.Dto;
using TD.Application.Services;
using TD.Presentation.Models;


namespace TD.Presentation.Controllers
{
    public class CustomerController : Controller
    {
        private readonly IAzureService _azureService;
        private readonly IMapper _mapper;

        public CustomerController(IAzureService azureService, IMapper mapper)
        {
            _azureService = azureService;
            _mapper = mapper;
        }

        // GET: Customer
        public async Task<ActionResult> Index()
        {
            var list = await _azureService.GetAllCustomersAsync();

            var modelList = list.Select(item => _mapper.Map<CustomerViewModel>(item))
                .ToList();

            return View(modelList);
        }

        // GET: Customer/Details/5
        public async Task<ActionResult> Details(Guid rowkey)
        {
            var dto = await _azureService.GetCustomerAsync(rowkey.ToString("N"));

            var model = _mapper.Map<CustomerViewModel>(dto);

            return View(model);
        }

        // GET: Customer/Create
        public ActionResult Create()
        {
            var model= new CustomerViewModel()
            {
                Active = true
            };
            model.RowKey = Guid.NewGuid().ToString("N");
         
            return View(model);
        }

        // POST: Customer/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([FromForm] CustomerViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            try
            {
                var entity = _mapper.Map<CustomerDto>(model);

                await _azureService.SaveCustomerAsync(entity);

                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                ViewBag.Exception = ex.Message;
                return View(model);
            }
        }

        // GET: Customer/Edit/5
        public async Task<ActionResult> Edit(Guid rowkey)
        {
            var entity = await _azureService.GetCustomerAsync(rowkey.ToString("N"));

            var model = _mapper.Map<CustomerViewModel>(entity);

            return View(model);
        }

        // POST: Customer/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([FromForm] CustomerViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            try
            {
                var dto = _mapper.Map<CustomerDto>(model);

                await _azureService.SaveCustomerAsync(dto);

                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                ViewBag.Exception = ex.Message;
                return View(model);
            }
        }

        // GET: Customer/Delete/5
        public async Task<ActionResult> Delete(Guid rowkey)
        {
            var dto = await _azureService.GetCustomerAsync(rowkey.ToString("N"));

            var model = _mapper.Map<CustomerViewModel>(dto);

            return View(model);
        }

        // POST: Customer/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete([FromForm] CustomerViewModel model)
        {
            try
            {
                var dto = _mapper.Map<CustomerDto>(model);

                await _azureService.DeleteCustomerAsync(dto);

                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                ViewBag.Exception = ex.Message;
                return View(model);
            }
        }
    }
}
