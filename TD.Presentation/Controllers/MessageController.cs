﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using TD.Application.Services;
using TD.Presentation.Helper;
using TD.Presentation.Models;

namespace TD.Presentation.Controllers
{
    public class MessageController : Controller
    {
        private readonly IAzureService _azureService;
        private readonly IReportService _reportService;
        private readonly IActionService _actionService;
        private readonly ISelectListService _selectListService;

        public MessageController(IAzureService azureService, 
            IReportService reportService, 
            IActionService actionService, ISelectListService selectListService)
        {
            _azureService = azureService;
            _reportService = reportService;
            _actionService = actionService;
            _selectListService = selectListService;
        }

        //public IActionResult Index()
        //{
        //    return View();
        //}

        public async Task<IActionResult> MessageSetup()
        {

            var model = new MessageViewModel
            {
                MessageTypeSelectList = _selectListService.GetMessageTypeSelectList(),
                ProfileNameSelectList =  await _selectListService.GetTestProfileSelectList()
            };

            return View(model);
        }

      

        public async Task<IActionResult> MessageStatusReport(string messageType, string profileName)
        {
            var profile = await _azureService.GetTestProfileByNameAsync(profileName);

            var statusList = await _reportService.GetTrackingNumberStatusAsync(profile.TrackingNumbers.Numbers);

            var list = TableHelper.BuildMessagePivot(statusList, messageType);

            var report = new StatusReport()
            {
                ProfileName = profile.ProfileName,
                ReportName = "Message Status Report",
                Datestamp = DateTime.Now,
                MessageType = messageType,
                ScanPivot = list
            };
            

            return View(report);
        }

        public async Task<IActionResult> SendMessage(string number, string messageType, string profileName)
        {
            //var list =
            
            var list = new List<string> { number };

            await _actionService.SendMessageToTrackAndTrace(list, messageType);
            
            return RedirectToAction("MessageSetup");
        }

        public async Task<IActionResult> SendAllMessage(List<string> numberlist, string messageType)
        {
            await _actionService.SendMessageToTrackAndTrace(numberlist, messageType);

            return RedirectToAction("MessageSetup");
        }
    }
}