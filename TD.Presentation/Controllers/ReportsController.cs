﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using TD.Application.Services;
using TD.Presentation.Helper;
using TD.Presentation.Models;

namespace TD.Presentation.Controllers
{
    public class ReportsController : Controller
    {
        private readonly IReportService _reportService;
        private readonly IAzureService _azureService;
        
        public ReportsController(IReportService reportService, IAzureService azureService)
        {
            _reportService = reportService;
            _azureService = azureService;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> SelectReport()
        {
            var model = new TestProfileViewModel {ProfileNameSelectList =  await GetProfileSelectList()};
            
            return View(model);
        }

        private async Task<SelectList> GetProfileSelectList()
        {

            var profiles = await _azureService.GetAllTestProfilesAsync();
            var items = profiles
                .Select(profile =>  new ListItem()
                {
                    Value = profile.ProfileName, Text = profile.ProfileName
                })
                .ToList();
            
            var list  = new SelectList(items, "Value", "Text");

            return list;
        }


        public async Task<IActionResult> DisplayStatusReport(string profileName)
        {
            var profile = await _azureService.GetTestProfileByNameAsync(profileName);

            //Lookup Scan results
            var statusList = await _reportService.GetTrackingNumberStatusAsync(profile.TrackingNumbers.Numbers);

            var list = TableHelper.BuildMessagePivot(statusList);

            var report = new StatusReport()
            {
                ProfileName = profile.ProfileName, 
                ReportName = "Scan Report", 
                Datestamp = DateTime.Now,
                ScanPivot = list
            };
            
            return View(report);
        }
        
        public async Task<IActionResult> DisplayIftminErrors()
        {
            var errors = await _reportService.GetIftminFileErrorsAync();

            var report = TableHelper.BuildIftminErrorTableData(errors);
            
            return View(report);
        }

        public async Task<IActionResult> DisplayIftmattErrors()
        {
            var errors = await _reportService.GetItmattFileErrorsAync();

            var report = TableHelper.BuildItmattErrorTableData(errors);

            return View(report);
        }
    }
}
