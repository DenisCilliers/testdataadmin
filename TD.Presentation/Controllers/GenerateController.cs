﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using TD.Application.Dto;
using TD.Application.Services;
using TD.Common.Config;
using TD.Common.Model;
using TD.Presentation.Helper;
using TD.Presentation.Models;

namespace TD.Presentation.Controllers
{
    public class GenerateController : Controller
    {
        private readonly IAzureService _azureService;
        private readonly IActionService _actionService;
        private GenerateConfig _generateConfig;
        private ISelectListService _selectListService;

        public GenerateController(IAzureService azureService, 
            IActionService actionService, 
            GenerateConfig generateConfig, 
            ISelectListService selectListService)
        {
            _azureService = azureService;
            _actionService = actionService;
            _generateConfig = generateConfig;
            _selectListService = selectListService;
            }

        //public IActionResult Index()
        //{
        //    return View();
        //}

        public async Task<IActionResult> SetupTrackingNumbers()
        {
            var model = new GenerateViewModel()
            {
                ProfileNameSelectList = await _selectListService.GetTestProfileSelectList(),
                VendorSelectList = await _selectListService.GetVendorSelectList(),
                FileName = _generateConfig.TrackingNumberFilename,
                NumberCount = 1
            };
            
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> GenerateTrackingNumbers(GenerateViewModel model)
        {
            var trackingNumberFilePath = _generateConfig.RootPath + "\\" + model.FolderPath+ "\\" + model.FileName;

            var dto = new GenerateDto()
            {
                NumberCount = model.NumberCount,
                Postfix = model.Postfix,
                Prefix = model.Prefix,
                StartNumber = model.StartNumber,
                TrackingNumberFilePath = trackingNumberFilePath
            };
            
            await _actionService.GenerateS10TrackingNumbers(dto);

            model.TrackingNumberFilePath = trackingNumberFilePath;
            model.FileNames = FileHelper.GetFilesInFolder(trackingNumberFilePath,"*.txt");

            return View(model);
        }

        
        public async Task<IActionResult> SetupThirdPartyBarcodes()
        {
            var model = new GenerateViewModel()
            {
                ProfileNameSelectList = await _selectListService.GetTestProfileSelectList(),
                VendorSelectList = await _selectListService.GetVendorSelectList(),
                FileName = _generateConfig.TrackingNumberFilename,
                NumberCount = 1
            };


            return View(model);
        }

        [HttpPost]
        public IActionResult GenerateThirdPartyBarcodes(GenerateViewModel model)
        {

            return View(model);
        }
        

        public async Task<JsonResult> GetProfileData(string rowkey)
        {
            if (rowkey == null) return null;

            var dto = await _azureService.GetTestProfileAsync(rowkey);

            var result = new JsonResult(dto);

            return result;
        }

        public async Task<JsonResult> GetVendorData(string rowkey)
        {
            if (rowkey == null) return null;

            var dto = await _azureService.GetVendorAsync(rowkey);

            var result = new JsonResult(dto);

            return result;
        }

        
    }
}