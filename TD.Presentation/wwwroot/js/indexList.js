﻿$(document).ready(function () {
    $('#IndexTable').DataTable({
        stateSave: true,
        scrollX: false,
        responsive: true,
        "order": [[2, "desc"]]
    });
});