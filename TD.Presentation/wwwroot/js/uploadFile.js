﻿$(document).ready(function() {

    $("#DefaultFolderName").prop('disabled', true);

    $("#ProfileName").change(function () {

        $("#DefaultFolderName").prop('disabled', false);

        var rowkey = $("#ProfileName").val();
        GetProfile(rowkey);
    });

});

function GetProfile(rowkey) {

    $.ajax({
        type: "GET",
        url: '/File/GetProfileData',
        data: { "rowkey": rowkey },
        dataType: "json",
        success: function (data) {

            console.log(data.profileName);
            var folderPath = data.folderPath;

            //console.log(FolderPath);
            $("#DefaultFolderName:text").val(folderPath);
           
        },
        error: function (ret) {
            var data = JSON.parse(ret.responseText);
            console.log('error');
            console.log(data);
        }
    });
}

