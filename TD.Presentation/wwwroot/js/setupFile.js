﻿$(document).ready(function () {

    //Disable Items
    $("#FolderPath").prop('disabled', true);
    $("#VendorKey").prop('disabled', true);
    $("#CustomerKey").prop('disabled', true);
    $("#ItemKey").prop('disabled', true);
    //$("#NumbersPerFile").prop('disabled', true);

    $("#ProfileName").change(function () {
        //Setup Dropdowns active
        $("#FolderPath").prop('disabled', false);
        $("#VendorKey").prop('disabled', false);
        $("#CustomerKey").prop('disabled', false);
        $("#ItemKey").prop('disabled', false);
        //$("#NumbersPerFile").prop('disabled', false);

        //Set the Options based on the values in the profile.
        var rowkey = $("#ProfileName").val();
        console.log("Profile :" + rowkey);

        GetProfile(rowkey);
    });
});


function GetProfile(rowkey) {

    $.ajax({
        type: "GET",
        url: '/File/GetProfileData',
        data: { "rowkey": rowkey },
        dataType: "json",
        success: function (data) {

            console.log(data.profileName);

            var folderPath = data.folderPath;
            var vendorKey = data.vendorKey;
            var customerKey = data.customerKey;
            var itemKey = data.itemKey;
            
            //console.log(FolderPath);
            
            $("#FolderPath:text").val(folderPath);
            $('#VendorKey').val(vendorKey);
            $('#CustomerKey').val(customerKey);
            $('#ItemKey').val(itemKey);
            
        },
        error: function (ret) {
            var data = JSON.parse(ret.responseText);
            console.log('error');
            console.log(data);
        }
    });
}