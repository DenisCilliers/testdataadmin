﻿function save() {
    console.log("Save Function Called.");

    var input = $('#trackingNumbersInput').val();

    //console.log(input);
    var numbers = input.split(/\r?\n/);

    //console.log(numbers);
    //convert to json & pretty
    var jsonString = `{"Numbers":${JSON.stringify(numbers)}}`;

    var obj = JSON.parse(jsonString);
    var pretty = JSON.stringify(obj, undefined, 4);

    $('#TrackingNumbersJson').val(pretty);

    $('#trackingNumberModal').modal('hide');

}


function prettyPrint() {
    var ugly = $('#TrackingNumbersJson').val();

    var obj = JSON.parse(ugly);
    var pretty = JSON.stringify(obj, undefined, 4);

    $('#TrackingNumbersJson').val(pretty);
}