﻿$(document).ready(function () {

    //Disable Items
    $("#FolderPath").prop('disabled', true);
    $("#VendorKey").prop('disabled', true);

    $("#NumberCount").prop('disabled', true);
    $("#FileName").prop('disabled', true);
    $("#StartNumber").prop('disabled', true);
    $("#Prefix").prop('disabled', true);
    $("#Postfix").prop('disabled', true);

    $("#ProfileRowkey").change(function () {
        //Setup Dropdowns active
        $("#FolderPath").prop('disabled', false);
        $("#VendorKey").prop('disabled', false);

        $("#NumberCount").prop('disabled', false);
        $("#FileName").prop('disabled', false);
        $("#StartNumber").prop('disabled', false);
        $("#Prefix").prop('disabled', false);
        $("#Postfix").prop('disabled', false);
        
        //Set the Options based on the values in the profile.
        var rowkey = $("#ProfileRowkey").val();
        console.log("Profile :" + rowkey);

        GetProfile(rowkey);
    });

    $("#VendorKey").change(function() {

        var rowkey = $("#VendorKey").val();
        console.log("Vendor :" + rowkey);

        GetVendor(rowkey);
    });
});


function GetProfile(rowkey) {

    $.ajax({
        type: "GET",
        url: '/Generate/GetProfileData',
        data: { "rowkey": rowkey },
        dataType: "json",
        success: function (data) {

            console.log(data.profileName);

            var folderPath = data.folderPath;
            var vendorKey = data.vendorKey;
            //var customerKey = data.customerKey;
            //var itemKey = data.itemKey;
            
            //console.log(outputPath);
            
            $("#FolderPath:text").val(folderPath);
            $('#VendorKey').val(vendorKey);
            //$('#SenderName').val(customerKey);
            //'#ItemType').val(itemKey);

            GetVendor(vendorKey);

        },
        error: function (ret) {
            var data = JSON.parse(ret.responseText);
            console.log('error');
            console.log(data);
        }
    });
}

function GetVendor(rowkey) {

    $.ajax({
        type: "GET",
        url: '/Generate/GetVendorData',
        data: { "rowkey": rowkey },
        dataType: "json",
        success: function (data) {

            console.log(data.vendorName);
            //console.log(FolderPath);

            $("#NumberCount:text").val(data.numberCount);
            $("#StartNumber:text").val(data.startNumber);
            $("#Prefix:text").val(data.prefix);
            $("#Postfix:text").val(data.postfix);

        },
        error: function (ret) {
            var data = JSON.parse(ret.responseText);
            console.log('error');
            console.log(data);
        }
    });
}