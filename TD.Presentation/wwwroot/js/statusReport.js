﻿$(document).ready(function () {
    $('#ReportTable').DataTable({
        stateSave: true,
        scrollX: false,
        responsive: true,
        "order": [[1, "desc"]]
    });
});