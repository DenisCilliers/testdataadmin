﻿using System;
using MCR.Infrastructure.TrackAndTrace.Service;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;
using TD.Application.Services;
using TD.Common.Config;
using TD.Infrastructure.Data.Azure;
using TD.Infrastructure.Data.Azure.Base;
using TD.Infrastructure.Data.Azure.Repository;
using TD.Infrastructure.Data.Oracle;
using TD.Presentation.Helper;

namespace TD.Presentation.Extensions
{
    public static class ServiceExtensions
    {
        public static void RegisterServices(this IServiceCollection services, IConfiguration configuration)
        {

            services.AddTransient(typeof(ILogger<>), typeof(Logger<>));

            services.AddTransient<IReportService, ReportService>();
            services.AddTransient<ISelectListService, SelectListService>();

            //Azure Items
            services.AddTransient<IAzureService, AzureService>();
            services.AddTransient<IAzureTableStorage, AzureTableStorage>();
            services.AddTransient<IAzureFileStorage, AzureFileStorage>();
            services.AddTransient<IAzureConfigService, AzureConfigService>();

            //Services
            services.AddTransient<ITrackAndTraceService, TrackAndTraceService>();
            services.AddTransient<IActionService, ActionService>();
            

            //repo
            services.AddTransient<IVendorRepository, VendorRepository>();
            services.AddTransient<ICustomerRepository, CustomerRepository>();
            services.AddTransient<IItemRepository, ItemRepository>();
            services.AddTransient<ITestProfileRepository, TestProfileRepository>();

            services.AddTransient<IOracleRepository, OracleRepository>();

        }

        public static void BindConfigurations(this IServiceCollection services, IConfiguration configuration)
        {
            var generateConfig = new GenerateConfig();
            configuration.Bind("GenerateConfig", generateConfig);
            services.AddSingleton(generateConfig);

            var oracleConfig = new OracleConfig();
            configuration.Bind("OracleConfig", oracleConfig);
            services.AddSingleton(oracleConfig);

            var iftminConfig = new IftminConfig();
            configuration.Bind("IftminConfig", iftminConfig);
            services.AddSingleton(iftminConfig);

            var trackAndTraceConfig = new TrackAndTraceConfig();
            configuration.Bind("TrackAndTraceConfig", trackAndTraceConfig);
            services.AddSingleton(trackAndTraceConfig);

            var azureConfig = new AzureConfig();
            configuration.Bind("AzureConfig", azureConfig);
            services.AddSingleton(azureConfig);

            //var CustomsDB = configuration.GetConnectionString("CustomsDB");
            //var OracleTT = configuration.GetConnectionString("OracleTT");
            //var OracleTTMyDelivery = configuration.GetConnectionString("OracleTTMyDelivery");

            //UploadHelper.Config = configuration;
            //OracleHelper.Config = configuration;
            //FileHelper.Config = configuration;
        }

        public static IServiceCollection AddSerilogServices(this IServiceCollection services)
        {
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                //.WriteTo.Console(restrictedToMinimumLevel: LogEventLevel.Information)
                .WriteTo.File("logs/TD.Presentation.log", rollingInterval: RollingInterval.Day)
                .CreateLogger();

            AppDomain.CurrentDomain.ProcessExit += (s, e) => Log.CloseAndFlush();

            return services.AddSingleton(Log.Logger);
        }
    }
}
