﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using TD.Application.Services;
using TD.Presentation.Models;

namespace TD.Presentation.Helper
{
    public interface ISelectListService
    {
        Task<IEnumerable<SelectListItem>> GetTestProfileSelectList();
        Task<IEnumerable<SelectListItem>> GetVendorSelectList();
        Task<IEnumerable<SelectListItem>> GetItemTypeSelectList();

        Task<IEnumerable<SelectListItem>> GetCustomerSelectList();

        IEnumerable<SelectListItem> GetMessageTypeSelectList();
        IEnumerable<SelectListItem> GetProcessTypeSelectList();
    }

    public class SelectListService: ISelectListService
    {
        private readonly IAzureService _azureService;

        public SelectListService(IAzureService azureService)
        {
            _azureService = azureService;
        }

        public async Task<IEnumerable<SelectListItem>> GetVendorSelectList()
        {
            var list = await _azureService.GetAllVendorsAsync();
            var items = list
                .Select(list => new ListItem()
                {
                    Value = list.RowKey,
                    Text = list.Name + (list.MyDelivery ? " - (Delivery)" : "") + " - " + list.Prefix
                })
                .ToList();

            var selectList = new SelectList(items, "Value", "Text");

            return selectList;
        }
        
        public async Task<IEnumerable<SelectListItem>> GetTestProfileSelectList()
        {
            var profiles = await _azureService.GetAllTestProfilesAsync();
            var items = profiles
                .Select(profile => new ListItem()
                {
                    Value = profile.RowKey,
                    Text = profile.ProfileName
                })
                .ToList();

            var selectList = new SelectList(items, "Value", "Text");

            return selectList;
        }
        
        public IEnumerable<SelectListItem> GetMessageTypeSelectList()
        {
            var items = new List<ListItem>
            {
                new() { Value = "l7-in", Text = "37 (L7) - Scan In" },
                new() { Value = "81-bag", Text = "81 (F4) - Bag Scan" },
                new() { Value = "81-item", Text = "81 (F4) - Item Scan" },
                new() { Value = "k3", Text = "57 (K3) - Into Storage" },
                new() { Value = "l7-out", Text = "37 (L7) - Scan Out" },
                new() { Value = "d5", Text = "73 (D5) - Assigned to Route" },
                new() { Value = "m", Text = "13 (M)  - Attempted Delivery" },
                new() { Value = "n", Text = "14 (N)  - Successful Delivery" },
                new() { Value = "r1", Text = "57 (R1) - Return to Sender" },
                new() { Value = "l7d5", Text = "(L7 + D5)" },
                new() { Value = "l7d5m", Text = "(L7 + D5 + M)" },
                new() { Value = "l7d5mn", Text = "(L7 + D5 + M + N)" }
            };


            var list = new SelectList(items, "Value", "Text");

            return list;
        }

        public IEnumerable<SelectListItem> GetProcessTypeSelectList()
        {
            var items = new List<ListItem>
            {
                new() { Value = "Iftmin", Text = "Iftmin" },
                new() { Value = "Itmatt", Text = "Itmatt" },
            };

            var list = new SelectList(items, "Value", "Text");

            return list;
        }

        public async Task<IEnumerable<SelectListItem>> GetItemTypeSelectList()
        {
            var list = await _azureService.GetAllItemsAsync();
            var items = list
                .Select(list => new ListItem()
                {
                    Value = list.RowKey,
                    Text = list.Description
                })
                .ToList();

            var selectList = new SelectList(items, "Value", "Text");

            return selectList;
        }

        public async Task<IEnumerable<SelectListItem>> GetCustomerSelectList()
        {
            var list = await _azureService.GetAllCustomersAsync();
            var items = list
                .Select(list => new ListItem()
                {
                    Value = list.RowKey,
                    Text = list.FullName
                })
                .ToList();

            var selectList = new SelectList(items, "Value", "Text");

            return selectList;
        }

    }
}
