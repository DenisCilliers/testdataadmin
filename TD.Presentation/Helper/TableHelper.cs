﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using TD.Domain.Model;
using TD.Presentation.Models;

namespace TD.Presentation.Helper
{
    public static class TableHelper
    {

        public static List<FileProcessError> BuildIftminErrorTableData(List<FileErrorStatus> errorList)
        {
            var report= new List<FileProcessError>();
          
            foreach (var error in errorList)
            {
                var detailList = error.Details.Split($"\n");
                var file = detailList[0].Replace("file=", "").Replace($"\n", " ");

                string line;
                if (detailList[2].Contains("file_line=PCI+18+"))
                {
                    line = detailList[2].Replace("file_line=PCI+18+", "");
                }
                else if (detailList[2].Contains("file_line=FTX+ZZZ+++"))
                {
                    line = detailList[2].Replace("file_line=FTX+ZZZ+++", "");
                }
                else
                {
                    line = detailList[2];
                }

                var fileError = new FileProcessError()
                {
                    DateStamp = error.ErrorDate, File = file, Line = line, Message = error.Message
                };

                report.Add(fileError);


            }

            if (!errorList.Any())
            {
                report.Add(new FileProcessError() { DateStamp = DateTime.Now, Message = "No records", Line = "", File = "" });
            }

            return report;
        }

        public static List<FileProcessError> BuildItmattErrorTableData(List<FileErrorStatus> errorList)
        {
            var report = new List<FileProcessError>();

            foreach (var error in errorList)
            {
                var detailList = error.Details.Split($"\n");

                var file = detailList[0].Replace("file=", "").Replace($"\n", "");

                var messageList = error.Message.Split($"\n");
                var message = messageList[0]
                    .Replace("ORA-31011: ", "")
                    .Replace("ORA-06510: PL/SQL:", "")
                    .Replace("ORA-06502: PL/SQL:", "");


                var fileError = new FileProcessError()
                {
                    DateStamp = error.ErrorDate,
                    File = file,
                    Message = message
                };

                report.Add(fileError);


            }

            if (!errorList.Any())
            {
                report.Add(new FileProcessError() { DateStamp = DateTime.Now, Message = "No records", Line = "", File = "" });
            }

            return report;
        }

        public static List<PivotItem> BuildMessagePivot(IEnumerable<ScanStatus> statusList)
        {
            var list = statusList.GroupBy(r => r.TrackingNumber)
                .Select(g => new PivotItem()
                {
                    Id = "",
                    No = g.Key,
                    ScanL5 = g.Any(x => x.ScanCode == "35") ? "*" : "",
                    ScanL7 = g.Any(x => x.ScanCode == "37") ? "*" : "",
                    ScanK3 = g.Any(x => x.ScanCode == "57") ? "*" : "",
                    ScanD5 = g.Any(x => x.ScanCode == "73") ? "*" : "",
                    ScanM = g.Any(x => x.ScanCode == "13") ? "*" : "",
                    ScanN = g.Any(x => x.ScanCode == "14") ? "*" : "",
                    //ScanR1 = g.Count(x => x.ScanCode == "56") > 0 ? "*" : "",
                    DateStamp = g.Max(x => x.ScanDate)

                })
                .OrderBy(o => o.No)
                .ToList();

         

            return list;
        }
        public static List<PivotItem> BuildMessagePivot(IEnumerable<ScanStatus> statusList, string messageType)
        {
            var list = statusList.GroupBy(r => r.TrackingNumber)
                .Select(g => new PivotItem()
                {
                    Id = "",
                    No = g.Key,
                    ScanL5 = g.Any(x => x.ScanCode == "35") ? "*" : "",
                    ScanL7 = g.Any(x => x.ScanCode == "37") ? "*" : "",
                    ScanK3 = g.Any(x => x.ScanCode == "57") ? "*" : "",
                    ScanD5 = g.Any(x => x.ScanCode == "73") ? "*" : "",
                    ScanM = g.Any(x => x.ScanCode == "13") ? "*" : "",
                    ScanN = g.Any(x => x.ScanCode == "14") ? "*" : "",
                    ScanR1 = g.Count(x => x.ScanCode == "56") > 0 ? "*" : "",
                    DateStamp = g.Max(x => x.ScanDate)

                })
                .OrderBy(o => o.No)
                .ToList();

            foreach (var item in list)
            {
                switch (messageType)
                {
                    case "l7-in":
                        if(string.IsNullOrEmpty(item.ScanL7)) item.ScanL7 = "?";
                        break;
                    //case "81-bag":
                    //    item.ScanF4 = "?";
                    //    break;
                    //case "81-item":
                    //    item.ScanF4 = "?";
                    //    break;
                    case "k3":
                         if (string.IsNullOrEmpty(item.ScanK3)) item.ScanK3 = "?";
                        break;
                    case "l7-out":
                        if (string.IsNullOrEmpty(item.ScanL7)) item.ScanL7 = "?";
                        break;
                    case "d5":
                        if (string.IsNullOrEmpty(item.ScanD5)) item.ScanD5 = "?";
                        break;
                    case "m":
                        if (string.IsNullOrEmpty(item.ScanM)) item.ScanM = "?";
                        break;
                    case "n":
                        if (string.IsNullOrEmpty(item.ScanN)) item.ScanN = "?";
                        break;
                    case "r1":
                        if (string.IsNullOrEmpty(item.ScanR1)) item.ScanR1 = "?";
                        break;

                    case "l7d5":
                        if (string.IsNullOrEmpty(item.ScanL7)) item.ScanR1 = "?";
                        if (string.IsNullOrEmpty(item.ScanD5)) item.ScanR1 = "?";
                        break;

                    case "l7dm":
                        if (string.IsNullOrEmpty(item.ScanL7)) item.ScanR1 = "?";
                        if (string.IsNullOrEmpty(item.ScanD5)) item.ScanR1 = "?";
                        if (string.IsNullOrEmpty(item.ScanM)) item.ScanM = "?";
                        break;
                    case "l7d5mn":
                        if (string.IsNullOrEmpty(item.ScanL7)) item.ScanR1 = "?";
                        if (string.IsNullOrEmpty(item.ScanD5)) item.ScanR1 = "?";
                        if (string.IsNullOrEmpty(item.ScanM)) item.ScanM = "?";
                        if (string.IsNullOrEmpty(item.ScanN)) item.ScanN = "?";
                        break;
                }

            }


            return list;
        }

    }
}
