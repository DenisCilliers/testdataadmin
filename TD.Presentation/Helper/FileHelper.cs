﻿using System;
using System.IO;
using TD.Common.Model;
using TD.Presentation.Models;

namespace TD.Presentation.Helper
{
    public class FileHelper
    {
        public static FileInfo[] GetFilesInFolder(string filepath, string search)
        {
            var path = Path.GetDirectoryName(filepath);
            var folder = new DirectoryInfo(path);

            var files = folder.GetFiles(search);
            
            return files;
        }

        public static CreateFileViewModel GetFilesInFolder(CreateFileDto dto, string search)
        {
            var path = Path.GetDirectoryName(dto.DefaultFolderName);
            var folder = new DirectoryInfo(path);

            var result = new CreateFileViewModel { DefaultFolderName = folder.FullName };

            try
            {
                result.FileNames = folder.GetFiles(search);
            }
            catch (Exception)
            {
                result.FileNames = Array.Empty<FileInfo>();
            }
            

            return result;
        }

    }
}
