﻿using AutoMapper;
using TD.Application.Dto;
using TD.Presentation.Models;

namespace TD.Presentation.Mapping
{
    public class AutoMapping : Profile
    {
        public AutoMapping()
        {
            CreateMap<TestProfileDto, TestProfileViewModel>()
                .ForMember(dest => dest.Active, act => act.MapFrom(src => src.Active))
                .ForMember(x => x.TrackingNumbers, opt => opt.Ignore())
                .ReverseMap();

            CreateMap<CustomerDto, CustomerViewModel>()
                .ForMember(dest => dest.Active, act => act.MapFrom(src => src.Active))
                .ReverseMap();

            CreateMap<VendorDto, VendorViewModel>()
                .ForMember(dest => dest.Active, act => act.MapFrom(src => src.Active))
                .ReverseMap();

            CreateMap<ItemDto, ItemViewModel>()
                .ForMember(dest => dest.Active, act => act.MapFrom(src => src.Active))
                .ReverseMap();
        }
    }
}
