﻿namespace TestData.Presentation.Extentions
{
    public static class ServiceExtensions
    {
        public static void RegisterServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<IOracleService, OracleService>();

        }

        public static void BindConfigurations(this IServiceCollection services, IConfiguration configuration)
        {
            //var generateConfig = new GenerateConfig();
            //configuration.Bind("GenerateConfig", generateConfig);
            //services.AddSingleton(generateConfig);

            var oracleConfig = new OracleConfig();
            configuration.Bind("OracleConfig", oracleConfig);
            services.AddSingleton(oracleConfig);

            //var iftminConfig = new IftminConfig();
            //configuration.Bind("IftminConfig", iftminConfig);
            //services.AddSingleton(iftminConfig);

            var trackAndTraceConfig = new TrackAndTraceConfig();
            configuration.Bind("TrackAndTraceConfig", trackAndTraceConfig);
            services.AddSingleton(trackAndTraceConfig);

            var azureConfig = new AzureConfig();
            configuration.Bind("AzureConfig", azureConfig);
            services.AddSingleton(azureConfig);

            //var CustomsDB = configuration.GetConnectionString("CustomsDB");
            //var OracleTT = configuration.GetConnectionString("OracleTT");
            //var OracleTTMyDelivery = configuration.GetConnectionString("OracleTTMyDelivery");

            //UploadHelper.Config = configuration;
            //OracleHelper.Config = configuration;
            //FileHelper.Config = configuration;
        }

        public static IServiceCollection AddSerilogServices(this IServiceCollection services)
        {
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                //.WriteTo.Console(restrictedToMinimumLevel: LogEventLevel.Information)
                .WriteTo.File("logs/MCR.Processor.TestDataGenerator.log", rollingInterval: RollingInterval.Day)
                .CreateLogger();

            AppDomain.CurrentDomain.ProcessExit += (s, e) => Log.CloseAndFlush();

            return services.AddSingleton(Log.Logger);
        }
    }
}
