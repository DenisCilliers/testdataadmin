﻿namespace TestData.Presentation.Extentions
{
    public static class StringExtensions
    {
        public static string Left(this string input, int characters)
        {
            if (characters < 0)
                throw new ArgumentOutOfRangeException();
            if (characters > input.Length)
                characters = input.Length;

            var result = input.Substring(0, characters);
            return result;
        }

        public static string Right(this string input, int characters)
        {
            if (characters < 0)
                throw new ArgumentOutOfRangeException();
            if (characters > input.Length)
                characters = input.Length;

            var result = input.Substring(input.Length - characters, characters);

            return result;
        }

        public static string Mid(this string input, int start, int count)
        {
            if (start < 0)
                throw new ArgumentOutOfRangeException();
            if (count < 0)
                throw new ArgumentOutOfRangeException();

            var stringStart = Math.Min(start, input.Length);
            var stringLen = Math.Min(count, Math.Max(input.Length - start, 0));

            var result = input.Substring(stringStart, stringLen);
            return result;
        }
    }
}
