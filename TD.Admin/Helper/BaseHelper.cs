﻿namespace TestData.Presentation.Helper
{
    public static class BaseHelper
    {
        public static void LogMessage(string message)
        {
            Console.WriteLine(message);
            Log.Debug(message);
        }


        public static void LogMessageNewLine(string message)
        {
            Console.WriteLine(" \r\n"+message);
            Log.Debug(message);
        }

        public static void LogMessageNewLine(string message, string methodName)
        {
            Console.WriteLine(" \r\n" + message);
            Log.Debug(methodName+"()");
        }
        
        public static void LogError(Exception exception, string message)
        {
            Console.WriteLine(" \r\n Error in " + message + ":" + exception.Message);
            Log.Error(exception, message);
        }

        public static void LogMethod(string methodName)
        {
            Log.Debug(methodName + "()");
        }

        public static void LogInformation(string message)
        {
            Log.Information(message);
        }
    }
}
