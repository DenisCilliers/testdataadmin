﻿namespace TestData.Presentation.Config
{
    public static class ConfigurationBuilderExtensions
    {
        /// <summary>
        /// augment configuration hierarchy to inject KeyVault Service
        /// this is driven of configuration under the key with the name driven by <paramref name="kvUrlConfigName"/> (defaulted to KEYVAULT_URL)
        /// 
        /// we re-add the environment variables source - this allows for KV values to be overriden (think local development)
        /// </summary>
        /// <param name="configurationBuilder">configuration builder instance</param>
        /// <param name="environment">name of the current environment</param>
        /// <param name="kvUrlConfigName">name of the configuration to hold optional KeyVault url</param>
        /// <param name="kvReloadSeconds">kv reload time span (defaulted to 5 mins)</param>
        /// <returns>configuration builder for chaining</returns>
        public static IConfigurationBuilder SetConfigSequence(this IConfigurationBuilder configurationBuilder, string environment = "production", string kvUrlConfigName = "KEYVAULT_URL", int kvReloadSeconds = 5 * 60)
        {
            var config = configurationBuilder.Build();

            var kvUrl = config[kvUrlConfigName];

            if (!string.IsNullOrWhiteSpace(kvUrl))
            {
                configurationBuilder.AddAzureKeyVault(new AzureKeyVaultConfigurationOptions
                {
                    Vault = kvUrl,
                    Client = new KeyVaultClient(new KeyVaultClient.AuthenticationCallback(new AzureServiceTokenProvider().KeyVaultTokenCallback)),
                    Manager = new DefaultKeyVaultSecretManager(),
                    ReloadInterval = TimeSpan.FromSeconds(kvReloadSeconds)
                });
            }

            configurationBuilder.AddEnvironmentVariables(); //this allows overrides (i.e. local development)

            return configurationBuilder;
        }
    }
}
