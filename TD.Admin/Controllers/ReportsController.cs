﻿using TestData.Presentation.Helper;

namespace TestData.Presentation.Controllers
{
    public class ReportsController : Controller
    {
        private IOracleService _oracleService;
        public ReportsController(IOracleService oracleService)
        {
            _oracleService = oracleService;
        }

        public IActionResult Index()
        {
            return View();
        }


        public async Task<IActionResult> Bulk20()
        {
            var runDir = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);

            var path = runDir+ @"\data\Bulk20_Numbers.txt";
            
            var trackingNumbers = FileHelper.LoadFile(path);

            //Lookup Scan results
            var statusList = await _oracleService.GetTrackingNumberStatusAsync(trackingNumbers);

            var list = BuildMessagePivot(statusList);

            
            return View(list);
        }


        private static List<PivotItem> BuildMessagePivot(IEnumerable<ScanStatus> statusList)
        {
           
            var list = statusList.GroupBy(r => r.TrackingNumber)
                .Select(g => new PivotItem()
                {
                    Id = g.Key,
                    ScanL5 = g.Count(x => x.ScanCode == "35") > 0 ? "*" : "",
                    ScanL7 = g.Count(x => x.ScanCode == "37") > 0 ? "*" : "",
                    ScanK3 = g.Count(x => x.ScanCode == "57") > 0 ? "*" : "",
                    ScanD5 = g.Count(x => x.ScanCode == "73") > 0 ? "*" : "",
                    ScanM = g.Count(x => x.ScanCode == "13") > 0 ? "*" : "",
                    ScanN = g.Count(x => x.ScanCode == "14") > 0 ? "*" : "",
                    //ScanR1 = g.Count(x => x.ScanCode == "56") > 0 ? "*" : "",
                    DateStamp = g.Max(x => x.ScanDate)

                })
                .OrderBy(o => o.Id)
                .ToList();

            return list;
        }
    }
}
