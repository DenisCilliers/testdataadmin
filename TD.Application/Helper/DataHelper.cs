﻿using System;
using System.Collections.Generic;
using IFTMIN.Engine.Constants;
using IFTMIN.Engine.Models;

namespace TD.Application.Helper
{
    public static class DataHelper
    {
        public static string GetRandomLocation()
        {
            var locations = new string[]
            {
                "CORABC000001",
                "CORDEF000014",
                "CORSDC000101",
                "CORAAC000002",
                "CORYUI000001",
                "COR123010098",
                "CORXDE000003",
                "CORAQW000017",
                "CORAAA000012",
                "CORABD000009",
                "RCKAAA000003",
                "RCKAAB000002",
                "RCKAAC001909",
                "RCKBAA000099",
                "RCKBTY000012",
                "RCKCCC000014",
                "RCKDDD000003",
                "RCKTTT000101",
                "RCKTTU000012",
                "RCKZZZ000019",
            };

            Random random = new Random();

            var index = random.Next(0, 19);

            return locations[index];

        }

        public static IEnumerable<Parcel> GenerateDummyData(IEnumerable<string> trackingNumbers, Vendor vendor, Customer customer, Item item, bool thirdParty = false)
        {
            var parcels = new List<Parcel>();

            foreach (var trackingNumber in trackingNumbers)
            {
                var parcel = new Parcel()
                {
                    Value = new Value()
                    {
                        Customs = 250,
                        PostagePaid = 0
                    },
                    Weight = 1,
                    TrackTraceNumber = thirdParty ? null : trackingNumber,
                    FormattedTrackTraceNumber = thirdParty ? null : trackingNumber,
                    ThirdPartyBarcode = thirdParty ? trackingNumber : null,
                    DDType = vendor.Type == "DDP" ? DDU_DDP.DDP : DDU_DDP.DDU,
                    Customer = customer,
                    Vendor = vendor
                };

                parcel.Items.Add(item);

                parcels.Add(parcel);
            }

            return parcels;
        }

        public static Parcel GenerateDummyParcel(string trackingNumber, Vendor vendor, Customer customer, Item item, bool thirdParty = false)
        {
            var parcel = new Parcel()
            {
                Value = new Value()
                {
                    Customs = 250,
                    PostagePaid = 0
                },
                Weight = 1,
                TrackTraceNumber = thirdParty ? null : trackingNumber,
                FormattedTrackTraceNumber = thirdParty ? null : trackingNumber,
                ThirdPartyBarcode = thirdParty ? trackingNumber : null,
                DDType = vendor.Type == "DDP" ? DDU_DDP.DDP : DDU_DDP.DDU,
                Customer = customer,
                Vendor = vendor
            };

            parcel.Items.Add(item);

            return parcel;
        }
    }
}
