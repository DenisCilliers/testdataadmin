﻿using System.Collections.Generic;
using ITMATT.Engine.Models;
using TD.Application.Dto;

namespace TD.Application.Helper
{
    public static class ConvertHelper
    {

        public static  Addressee GetAddresseeFromCustomer(CustomerDto customer)
        {
            var addressee = new Addressee()
            {
                Role = "CN",
                Identification = new Identification() { Name = customer.FullName, Reference = customer.RowKey },
                PostalAddress = new PostalAddress()
                {
                    Premises = new List<string>() { customer.Address1, customer.Address2, customer.Address3, customer.Address4 },
                    Locality = new Locality() { Name = customer.City, Code = customer.PostalCode },
                    CountryCd = customer.Country
                },
                Contact = new Contact() { Email = customer.Email, Telephone = customer.Phone }
            };
            return addressee;
        }

        public static Sender GetSenderFromVendor(VendorDto vendor)
        {
            var sender = new Sender()
            {
                Role = "CZ",
                Identification = new Identification() { Name = vendor.Name, Reference = vendor.IFSAccount },
                PostalAddress = new PostalAddress()
                {
                    Premises = new List<string>() { vendor.Address1, vendor.Address2, vendor.Address3, vendor.Address4 },
                    Locality = new Locality() { Name = vendor.City, Code = vendor.PostCode },
                    CountryCd = vendor.Country
                },
                Contact = new Contact() { Email = vendor.Email, Telephone = vendor.PhoneNumber }
            };
            return sender;
        }

        public static ContentPiece GetContentPieceFromItem(ItemDto item)
        {
            var package = new ContentPiece()
            {
                Number = item.Quantity.ToString(),
                NumberOfUnits = "1",
                Description = item.Description,
                DeclaredValue = new DeclaredValue() { Currency = item.Currency, Amount = item.Price.ToString("F") },
                NetWeight = item.Weight.ToString("F"),
                OriginLocationCode = item.CountryOfOrigin,
                TariffHeading = item.ItemTariff
            };

            return package;
        }
    }
}
