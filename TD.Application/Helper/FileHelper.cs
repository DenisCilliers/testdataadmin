﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Microsoft.Extensions.Configuration;

namespace TD.Application.Helper
{
    public static class FileHelper
    {
        public static IConfiguration Config;
      

        public static async Task WriteFile(IEnumerable<string> contents, string fileName)
        {
            //Log.Debug($"WriteFile() {fileName}");

            try
            {
                var path = Path.GetDirectoryName(fileName);

                if (!Directory.Exists(path))
                {
                    // Try to create the directory.
                    var di = Directory.CreateDirectory(path);
                }

                var file = new StreamWriter(fileName, false, Encoding.Default);

                foreach (var s in contents)
                {
                    await file.WriteLineAsync(s);
                }

                

                file.Close();

            }
            catch (IOException ioex)
            {
                Console.WriteLine(ioex.Message);
            }
          
        }

        public static void WriteXmlFile(string xmlstring, string fileName)
        {
            //Log.Debug($"WriteFile() {fileName}");

            try
            {
                var path = Path.GetDirectoryName(fileName);

                if (!Directory.Exists(path))
                {
                    // Try to create the directory.
                    var di = Directory.CreateDirectory(path);
                }

                var doc = new XmlDocument();
                doc.LoadXml(xmlstring);
                doc.Save(fileName);

            }
            catch (IOException ioex)
            {
                Console.WriteLine(ioex.Message);
            }

        }

        public static List<string> LoadFile(string fileName)
        {
            //Log.Debug($"LoadFile() {fileName}");

            var contents = new List<string>();

            if (!File.Exists(fileName)) return contents;
            using var sr = new StreamReader(fileName, Encoding.Default);
            while (sr.Peek() >= 0)
            {
                var line = sr.ReadLine();
                if(!string.IsNullOrWhiteSpace(line)) 
                    contents.Add(line);
            }

            return contents;
        }


    }
}
