﻿using System;
using System.Text.RegularExpressions;

namespace TD.Application.Helper
{
    public static class TrackingNumberHelper
    {
        public static int TrackingNumberLength = 13;

        public static string ConstructTrackingNumber(string productCode, long itemNumber, string countryCode)
        {
            var padding = TrackingNumberLength - (productCode.Length + countryCode.Length);

            var trackingNumber = productCode + itemNumber.ToString().PadLeft(padding, '0') + countryCode;

            return trackingNumber;
        }

        public static bool IsValid(string trackingNumber)
        {
            try
            {
                trackingNumber = trackingNumber.Trim();

                if (trackingNumber.Length == 0)
                    return true;

                // Ensure that the trackNumber starts and ends with 2 letters and has 1 to 9 digits in the middle
                if (!Regex.IsMatch(trackingNumber, "^[a-zA-Z]{2}[0-9]{1,9}[a-zA-Z]{2}$"))
                {
                    return false;
                }

                var checkDigit = int.Parse(trackingNumber.Substring(trackingNumber.Length - 3, 1));
                var number = trackingNumber.Substring(2, trackingNumber.Length - 5);

                return CalculateCheckDigit(number) == checkDigit;
            }
            catch
            {
                return false;
            }
        }

        public static int CalculateCheckDigit(string number)
        {
            var sum = 0;
            var index = 0;
            var weights = new[] { 7, 9, 5, 3, 2, 4, 6, 8, 0, 0, 0, 0, 0, 0 };

            int calculatedCheckDigit;
            {
                for (int i = number.Length - 1; i >= 0; i--)
                {
                    sum = sum + (Convert.ToInt16(number.Substring(i, 1)) * weights[index]);
                    index++;
                }

                calculatedCheckDigit = 11 - (sum % 11);

                if (calculatedCheckDigit == 10)
                    calculatedCheckDigit = 0;
                else if (calculatedCheckDigit == 11)
                    calculatedCheckDigit = 5;
            }
            return calculatedCheckDigit;
        }

        /// <summary>
        /// deconstructs/parses S10 into constituents
        /// e.g. 
        /// 
        /// Product Code
        /// Item Number
        /// Country Code
        /// </summary>
        /// <param name="s10">S10 compliant code</param>
        /// <returns>constituents</returns>
        public static  (string productCode, long item, string countryCode) Deconstruct(string s10)
        {
            if (string.IsNullOrWhiteSpace(s10) || s10.Length != TrackingNumberLength)
                throw new ArgumentException("invalid s10 item reference", nameof(s10));

            return (s10.Substring(0, 2), long.Parse(s10.Substring(2, 9)), s10.Substring(11, 2));
        }

    }
}
