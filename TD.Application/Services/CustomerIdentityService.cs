﻿using System.Net;
using System.Threading.Tasks;
using MCR.Processor.TestDataGenerator.Model;
using Newtonsoft.Json;
using RestSharp;

namespace TD.Application.Services
{
    public interface ICustomerIdentityService
    {
        Task<string> GetJwtToken(string uid);
    }

    public class CustomerIdentityService: ICustomerIdentityService
    {
        const string AccountsUrl = "https://accounts.eu1.gigya.com";
        const string ApiKey = "_aYi1O3N2SydmD917c8E-w6TULbMHJUf7EKWDRqdter6OH-qYzf3j4qceuJNEsAqs";
        const string Secret = "fN4EnCy85L1bmwyVZH1iTV64sdegDUJ/";
        const string UserKey = "AKx59IuszK4p";

        public CustomerIdentityService()
        {
            
        }
        
        public async Task<string> GetJwtToken(string uid)
        {
            //uid = 03567d53e5604d66b8c0c33db0a1dbf6
            var proxy = WebRequest.DefaultWebProxy;

            proxy.Credentials = CredentialCache.DefaultCredentials;

            var client = new RestClient($"{AccountsUrl}/accounts.getJWT?" +
                                        $"apiKey={ApiKey}" +
                                        $"&secret={Secret}" +
                                        $"&userKey={UserKey}" +
                                        $"targetUID={uid}")
            {
                Proxy = proxy,
                Timeout = -1
            };

            var request = new RestRequest(Method.GET);
        
            //fields:

             var body = @"";
            request.AddParameter("text/plain", body, ParameterType.RequestBody);
            var response = await client.ExecuteAsync(request);

            var data = JsonConvert.DeserializeObject<Token>(response.Content);

            var token = data.Id_Token;

            return token;
        }
        
    }
}
