﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RestSharp;
using TD.Application.Services;
using TD.Domain.Entities.Model.Customs;

namespace MCR.Processor.TestDataGenerator.Services
{
    public interface ICustomsService
    {
        Task<List<CustomsStatus>> GetCustomsReferenceAsync(List<string> trackingNumber);
        Task<CustomsStatus> GetCustomsReferenceAsync(string trackingNumber);
        Task<bool> PayCustomsCharge(string customsReference);
    }

    public class CustomsService: ICustomsService
    {
        private const string CustomsAPIUrl = "https://anpostmailscustomsmydeliveryapidev.azurewebsites.net/api";
        const string LookupChargesEndpoint = "/lookuplist";
        const string PaidChargesEndpoint = "/paid";
        const string Uid = "03567d53e5604d66b8c0c33db0a1dbf6";

        private readonly ICustomerIdentityService CustomerIdentityService;

        public CustomsService(ICustomerIdentityService customerIdentityService )
        {
            CustomerIdentityService = customerIdentityService;
        }

        public async Task<List<CustomsStatus>> GetCustomsReferenceAsync(List<string> trackingNumbers)
        {
            Console.WriteLine($"* Checking Track&Trace Scan Status: {trackingNumbers.Count} \r\n");
            
            var token = CustomerIdentityService.GetJwtToken(Uid);
            var url = CustomsAPIUrl + LookupChargesEndpoint;

            var client = new RestClient(url) { Timeout = -1 };
            var request = new RestRequest(Method.POST);
            request.AddHeader("Authorization", $"Bearer {token}");
            request.AddHeader("Content-Type", "application/json");

            var lookup = new CustomsLookupRequest() { AnPostTrackingNumbers = trackingNumbers};
            var body = JsonConvert.SerializeObject(lookup);

            request.AddParameter("application/json", body, ParameterType.RequestBody);
            IRestResponse response = await client.ExecuteAsync(request);

            var data = JsonConvert.DeserializeObject<CustomsLookupResponse>(response.Content);

            var returnList =  data.PaymentLookupDetails
                .Select(item => new CustomsStatus()
                {
                    TrackingNumber = item.AnPostPackageTrackingNumber, 
                    RevenueMRN = item.RevenueMrn
                })
                .ToList();

            return returnList;
        }

        public async Task<CustomsStatus> GetCustomsReferenceAsync(string trackingNumber)
        {
            var token = CustomerIdentityService.GetJwtToken(Uid);
            var url = CustomsAPIUrl + LookupChargesEndpoint;

            var client = new RestClient(url) {Timeout = -1};
            var request = new RestRequest(Method.POST);
            request.AddHeader("Authorization", $"Bearer {token}");
            request.AddHeader("Content-Type", "application/json");

            var lookup = new CustomsLookupRequest() {AnPostTrackingNumbers = new List<string>() {trackingNumber}};
            var body = JsonConvert.SerializeObject(lookup);

            request.AddParameter("application/json", body, ParameterType.RequestBody);
            IRestResponse response = await client.ExecuteAsync(request);

            var data = JsonConvert.DeserializeObject<CustomsLookupResponse>(response.Content);

            var item = new CustomsStatus()
            {
                TrackingNumber = trackingNumber,
                RevenueMRN = data.PaymentLookupDetails[0].RevenueMrn
            };

            return item;
        }

        public async Task<bool> PayCustomsCharge(string customsReference)
        {
            var url = CustomsAPIUrl + PaidChargesEndpoint;

            var token = CustomerIdentityService.GetJwtToken(Uid);

            var client = new RestClient(url) { Timeout = -1 };
            var request = new RestRequest(Method.POST);
            request.AddHeader("Authorization", $"Bearer {token}");
            request.AddHeader("Content-Type", "application/json");

            var paid = new CustomsPaidRequest() { RevenueMrn = customsReference ,CustomerId = Uid};
            var body = JsonConvert.SerializeObject(paid);

            request.AddParameter("application/json", body, ParameterType.RequestBody);
            IRestResponse response = await client.ExecuteAsync(request);

            var data = JsonConvert.DeserializeObject<CustomsLookupResponse>(response.Content);

            return data.Success;
        }

    }
}
