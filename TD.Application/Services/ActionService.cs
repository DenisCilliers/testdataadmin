﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using IFTMIN.Engine;
using IFTMIN.Engine.Models;
using MCR.Infrastructure.TrackAndTrace.Service;
using Microsoft.Extensions.Logging;
using TD.Application.Dto;
using TD.Application.Helper;
using TD.Application.Map;
using TD.Common.Config;
using TD.Common.Model;

namespace TD.Application.Services
{
    public interface IActionService
    {
        Task SendMessageToTrackAndTrace(List<string> trackingNumbers, string message);

        Task GenerateS10TrackingNumbers(GenerateDto model);

        Task<CreateFileDto> CreateIftminFilesAsync(TestProfileDto testProfile);

        Task<CreateFileDto> CreateIftminFileRandomItemsAsync(TestProfileDto testProfile);
        Task<CreateFileDto> CreateItmattFilesAsync(TestProfileDto testProfile);
        Task<CreateFileDto> CreateItmattFilesRandomItemsAsync(TestProfileDto testProfile);
    }

    public class ActionService : IActionService
    {
        private readonly ITrackAndTraceService _trackAndTraceService;
        private readonly IAzureService _azureService;
        private readonly IReportService _reportService;
        private readonly ILogger<ActionService> _logger;

        private readonly IftminConfig _iftminConfig;
        private readonly GenerateConfig _generateConfig;

        public ActionService(ITrackAndTraceService trackAndTraceService, 
            ILogger<ActionService> logger, 
            IftminConfig iftminConfig, IAzureService azureService, GenerateConfig generateConfig, IReportService reportService)
        {
            _trackAndTraceService = trackAndTraceService;
            _logger = logger;
            _iftminConfig = iftminConfig;
            _azureService = azureService;
            _generateConfig = generateConfig;
            _reportService = reportService;
        }

        public async Task SendMessageToTrackAndTrace(List<string> trackingNumbers, string message)
        {
            try
            {
                _logger.LogInformation("\r\nSend Message To TrackAndTrace");

                var recordsPerMessage = _iftminConfig.RecordsPerMessage;
                var ifsAccount = _iftminConfig.VendorAccount;
                var thirdParty = _iftminConfig.UseThirdPartyBarcodeFile;
                

                while (trackingNumbers.Any())
                {
                    var subList = trackingNumbers.GetRange(0, recordsPerMessage > trackingNumbers.Count ? trackingNumbers.Count : recordsPerMessage);
                    trackingNumbers.RemoveRange(0, recordsPerMessage > trackingNumbers.Count ? trackingNumbers.Count : recordsPerMessage);

                    var location =  DataHelper.GetRandomLocation();
                    long? companyId = null;
                    if (!string.IsNullOrEmpty(ifsAccount))
                    {
                        companyId = long.Parse(ifsAccount);
                    }

                    //Log Message Send
                    foreach (var item in subList)
                    {
                        _logger.LogDebug($"* Sending Message for: {item}");
                    }

                    var messageResponse = false;

                    switch (message.ToLower())
                    {
                        case "l7-in":
                            messageResponse = await _trackAndTraceService.SendL7ToTandT(subList, companyId, true, thirdParty);
                            break;
                        case "81-bag":
                            messageResponse = await _trackAndTraceService.SendArrivalNotificationToTandT(subList, companyId, true, thirdParty);
                            break;
                        case "81-item":
                            messageResponse = await _trackAndTraceService.SendArrivalNotificationToTandT(subList, companyId, false, thirdParty);
                            break;
                        case "k3":
                            messageResponse = await _trackAndTraceService.SendK3ToTandT(subList, companyId, location, thirdParty);
                            break;
                        case "l7-out":
                            messageResponse = await _trackAndTraceService.SendL7ToTandT(subList, companyId, false, thirdParty);
                            break;
                        case "d5":
                            messageResponse = await _trackAndTraceService.SendD5ToTandT(subList, companyId, false, thirdParty);
                            break;
                        case "m":
                            messageResponse = await _trackAndTraceService.SendMToTandT(subList, companyId, false, thirdParty);
                            break;
                        case "n":

                            foreach (var number in subList)
                            {
                                await _trackAndTraceService.SendNToTandT(number, companyId, false, thirdParty);
                            }

                            break;
                        case "a":
                            messageResponse = await _trackAndTraceService.SendAToTandT(subList, companyId, false, thirdParty);
                            break;

                        case "r1":
                            messageResponse = await _trackAndTraceService.SendR1ToTandT(subList, companyId, false, thirdParty);
                            break;

                        case "l7d5":
                            await _trackAndTraceService.SendL7ToTandT(subList, companyId, true, thirdParty);
                            messageResponse = await _trackAndTraceService.SendD5ToTandT(subList, companyId, false, thirdParty);
                            break;
                        case "l7d5m":
                            await _trackAndTraceService.SendL7ToTandT(subList, companyId, true, thirdParty);
                            await _trackAndTraceService.SendD5ToTandT(subList, companyId, false, thirdParty);
                            messageResponse = await _trackAndTraceService.SendMToTandT(subList, companyId, false, thirdParty);
                            break;
                        case "l7d5mn":
                            await _trackAndTraceService.SendL7ToTandT(subList, companyId, true, thirdParty);
                            await _trackAndTraceService.SendD5ToTandT(subList, companyId, false, thirdParty);
                            await _trackAndTraceService.SendMToTandT(subList, companyId, false, thirdParty);
                            foreach (var number in subList)
                            {
                                await _trackAndTraceService.SendNToTandT(number, companyId, false, thirdParty);
                            }
                            break;
                    }

                    _logger.LogDebug(messageResponse ? "* Message Sent Successfully." : "* Error Sending Message.");
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "SendMessageToTrackAndTrace Error");
            }

        }

        public async Task GenerateS10TrackingNumbers(GenerateDto model)
        {
            _logger.LogInformation($"Generate S10 Tracking Numbers");

            try
            {
                var count = model.NumberCount;
                var prefix = model.Prefix;
                var postfix = model.Postfix;
                var startNumber = model.StartNumber;
                var file = model.TrackingNumberFilePath;

                _logger.LogInformation($"* Filename: {file}");

                //Prepare List for Checking in Oracle
                var checkList = new List<string>();

                for (var i = startNumber; i < startNumber + count; i++)
                {
                    var number = i.ToString().PadLeft(8, '0');
                    var fullNumber = prefix + number + TrackingNumberHelper.CalculateCheckDigit(number) + postfix;
                    checkList.Add(fullNumber);
                }

                var usedList = await _reportService.CheckTrackingNumbersAsync(checkList);

                var finalList = new List<string>();

                finalList.AddRange(from number in checkList
                    let check = usedList.Contains(number)
                    where !check
                    select number);

                foreach (var number in usedList)
                {
                    _logger.LogInformation($"* Tracking Number Used: {number}");
                }

                _logger.LogInformation($"* Tracking Numbers Generated. Count: {finalList.Count}");

                await FileHelper.WriteFile(finalList, file);
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"GenerateS10TrackingNumbers: Exception");
                throw;
            }
          
        }


        public async Task<CreateFileDto> CreateIftminFilesAsync(TestProfileDto testProfile)
        {
            _logger.LogInformation("Create IFTMIN Files");

            try
            {
                var result = new CreateFileDto() { };

                var recordsPerFile = _iftminConfig.RecordsPerFile;
                var iftminFile = _generateConfig.RootPath + "\\" + testProfile.FolderPath + "\\" + _generateConfig.DefaultIftminFilename.Replace("Default", testProfile.ProfileName);
                
                var trackingNumbers = testProfile.TrackingNumbers.Numbers;

                var item = await _azureService.GetItemAsync(testProfile.ItemKey);
                var vendor = await _azureService.GetVendorAsync(testProfile.VendorKey);
                var customer = await _azureService.GetCustomerAsync(testProfile.CustomerKey);


                //Package Items
                var parcels = DataHelper.GenerateDummyData(trackingNumbers, vendor.ToEntity(), customer.ToEntity(), item.ToEntity())
                    .ToList();

                _logger.LogInformation($"* Parcel Count: {parcels.Count}");

                var iftminParams = new EngineParameters()
                {
                    IFSAccount = vendor.IFSAccount,
                    Application = "MCR",
                    RecipientIdentification = "CUSTOMS",
                    SequenceNumber = "123"
                };

                var generator = new FileGenerator();

                var dirName = Path.GetDirectoryName(iftminFile);
                var file = Path.GetFileNameWithoutExtension(iftminFile);
                var ext = Path.GetExtension(iftminFile);

                var i = 1;
                while (parcels.Any())
                {
                    var subList = parcels.GetRange(0, recordsPerFile > parcels.Count ? parcels.Count : recordsPerFile);
                    parcels.RemoveRange(0, recordsPerFile > parcels.Count ? parcels.Count : recordsPerFile);

                    iftminParams.Date = DateTime.Now;

                    var sb = generator.GenerateIFTMINFile(subList, iftminParams);

                    var fileName = $"{dirName}\\{file}-{i}{ext}";

                    _logger.LogInformation($"* Filename     : {Path.GetFileName(fileName)}");
                    _logger.LogInformation($"* Parcel Count : {subList.Count}");

                    await FileHelper.WriteFile(new List<string>() { sb.ToString() }, fileName);

                    i++;
                }

                result.DefaultFolderName = iftminFile;
                
                return result;
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"CreateIftminFilesAsync: Exception");
                throw;
            }
            
        }
        public async Task<CreateFileDto> CreateIftminFileRandomItemsAsync(TestProfileDto testProfile)
        {
            _logger.LogInformation("Create Iftmin Files With Random Items");

            try
            {
                var result = new CreateFileDto() { };

                var recordsPerFile = _iftminConfig.RecordsPerFile;
                //var iftminFile = _generateConfig.IftminFilePath;
                var iftminFile = _generateConfig.RootPath + "\\" + testProfile.FolderPath + "\\" 
                                 + _generateConfig.DefaultIftminFilename.Replace("Default", testProfile.ProfileName);

                var trackingNumbers = testProfile.TrackingNumbers.Numbers;

                var vendor = await _azureService.GetVendorAsync(testProfile.VendorKey);
                var customer = await _azureService.GetCustomerAsync(testProfile.CustomerKey);

                //Package Items
                var parcels = 
                    (from trackingNumber in trackingNumbers
                        let item =  _azureService.GetRandomItemAsync().Result
                     select DataHelper.GenerateDummyParcel(trackingNumber, vendor.ToEntity(), customer.ToEntity(), item.ToEntity()))
                    .ToList();


                _logger.LogInformation($"* Parcel Count: {parcels.Count}");

                var iftminParams = new EngineParameters()
                {
                    IFSAccount = vendor.IFSAccount,
                    Application = "CUSTOMS",
                    RecipientIdentification = vendor.IFSAccount,
                    SequenceNumber = "123"
                };

                var generator = new FileGenerator();

                var dirName = Path.GetDirectoryName(iftminFile);
                var file = Path.GetFileNameWithoutExtension(iftminFile);
                var ext = Path.GetExtension(iftminFile);

                var i = 1;
                while (parcels.Any())
                {
                    var subList = parcels.GetRange(0, recordsPerFile > parcels.Count ? parcels.Count : recordsPerFile);
                    parcels.RemoveRange(0, recordsPerFile > parcels.Count ? parcels.Count : recordsPerFile);

                    iftminParams.Date = DateTime.Now;

                    var sb = generator.GenerateIFTMINFile(subList, iftminParams);

                    var fileName = $"{dirName}\\{file}-{i}{ext}";

                    _logger.LogInformation($"* Filename     : {Path.GetFileName(fileName)}");
                    _logger.LogInformation($"* Parcel Count : {subList.Count}");

                    await FileHelper.WriteFile(new List<string>() { sb.ToString() }, fileName);

                    i++;
                }

                result.DefaultFolderName = iftminFile;
                
                return result;
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"CreateIftminFileRandomItemsAsync: Exception");
                throw;
            }
        }

        public async Task<CreateFileDto> CreateItmattFilesAsync(TestProfileDto testProfile)
        {
            _logger.LogInformation("Create ITMATT Files");
            try
            {
                var result = new CreateFileDto() { };

                //testProfile.BuildTrackingNumbers();
                var trackingNumbers = testProfile.TrackingNumbers.Numbers;

                //var itmatFile = _generateConfig.ItmattFilePath;
                //var itmatFile = _generateConfig.RootPath + "\\" + _generateConfig.DefaultFolderName + "\\" + _generateConfig.DefaultItmattFilename;
                var itmatFile = _generateConfig.RootPath + "\\" + testProfile.FolderPath + "\\" + _generateConfig.DefaultItmattFilename;
                
                var currentItem = await _azureService.GetItemAsync(testProfile.ItemKey);
                var currentVendor = await _azureService.GetVendorAsync(testProfile.VendorKey);
                var currentCustomer = await _azureService.GetCustomerAsync(testProfile.CustomerKey);

                var item = ConvertHelper.GetContentPieceFromItem(currentItem);
                var sender = ConvertHelper.GetSenderFromVendor(currentVendor);
                var addressee = ConvertHelper.GetAddresseeFromCustomer(currentCustomer);

                //Convert to Itmatt Items

                var generator = new ITMATT.Engine.FileGenerator();
                //var rootPath = GenerateConfig.RootPath;

                for (var index = 0; index < trackingNumbers.Count; index++)
                {
                    var number = trackingNumbers[index];

                    //Generate the XML
                    var file = generator.GenerateItmattFile(sender, addressee, item, number);

                    //Prep Filename
                    var template = _generateConfig.DefaultFolderName + "_" + (index + 1).ToString().PadLeft(4, '0') + ".xml";
                    var fileName = itmatFile.Replace("Default.xml", template);

                    var outputFile = Path.GetFileName(fileName);
                    _logger.LogInformation($"* Output File : { outputFile}");

                    //Write the XML
                    FileHelper.WriteXmlFile(file, fileName);
                }

                result.DefaultFolderName = itmatFile;
                
                return result;
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"CreateItmattFilesAsync: Exception");
                throw;
            }
           
        }

        public async Task<CreateFileDto> CreateItmattFilesRandomItemsAsync(TestProfileDto testProfile)
        {
            _logger.LogInformation("Create ITMATT Files");
            try
            {
                var result = new CreateFileDto() { };

                //testProfile.BuildTrackingNumbers();
                var trackingNumbers = testProfile.TrackingNumbers.Numbers;

                //var itmatFile = _generateConfig.ItmattFilePath;
                //var itmatFile = _generateConfig.RootPath + "\\" + _generateConfig.DefaultFolderName + "\\" + _generateConfig.DefaultItmattFilename;
                var itmatFile = _generateConfig.RootPath + "\\" + testProfile.FolderPath + "\\" + _generateConfig.DefaultItmattFilename;

                var currentVendor = await _azureService.GetVendorAsync(testProfile.VendorKey);
                var currentCustomer = await _azureService.GetCustomerAsync(testProfile.CustomerKey);

                var sender = ConvertHelper.GetSenderFromVendor(currentVendor);
                var addressee = ConvertHelper.GetAddresseeFromCustomer(currentCustomer);

                //Convert to Itmatt Items

                var generator = new ITMATT.Engine.FileGenerator();
                //var rootPath = GenerateConfig.RootPath;

                for (var index = 0; index < trackingNumbers.Count; index++)
                {
                    var number = trackingNumbers[index];

                    var radItem = await _azureService.GetRandomItemAsync();
                    var item = ConvertHelper.GetContentPieceFromItem(radItem);

                    //Generate the XML
                    var file = generator.GenerateItmattFile(sender, addressee, item, number);

                    //Prep Filename
                    var template = _generateConfig.DefaultFolderName + "_" + (index + 1).ToString().PadLeft(4, '0') + ".xml";
                    var fileName = itmatFile.Replace("Default.xml", template);

                    var outputFile = Path.GetFileName(fileName);
                    _logger.LogInformation($"* Output File : { outputFile}");

                    //Write the XML
                    FileHelper.WriteXmlFile(file, fileName);
                }

                result.DefaultFolderName = itmatFile;
                
                return result;
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"CreateItmattFilesRandomItemsAsync: Exception");
                throw;
            }
            
        }

     

    }
}
