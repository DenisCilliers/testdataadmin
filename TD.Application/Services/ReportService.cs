﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using TD.Domain.Model;
using TD.Infrastructure.Data.Oracle;

namespace TD.Application.Services
{
    public interface IReportService
    {
        Task<bool> TestConnectionAsync();

        Task<List<string>> CheckTrackingNumbersAsync(List<string> trackingNumbers);

        Task<IEnumerable<ScanStatus>> GetTrackingNumberStatusAsync(List<string> trackingNumbersNumbers);

        Task<List<FileErrorStatus>> GetIftminFileErrorsAync();

        Task<List<FileErrorStatus>> GetItmattFileErrorsAync();
    }

    public class ReportService: IReportService
    {
        private readonly IOracleRepository _oracleRepository;
        private readonly ILogger<ReportService> _logger;

        public ReportService(IOracleRepository oracleRepository, ILogger<ReportService> logger)
        {
            _oracleRepository = oracleRepository;
            _logger = logger;
        }

        public async Task<bool> TestConnectionAsync()
        {
            var result = await _oracleRepository.TestConnectionAsync();

            return result;
        }

        public async Task<List<string>> CheckTrackingNumbersAsync(List<string> trackingNumbers)
        {
            var result = await _oracleRepository.CheckTrackingNumbersAsync(trackingNumbers);
            return result;
        }


        public async Task<IEnumerable<ScanStatus>> GetTrackingNumberStatusAsync(List<string> trackingNumbersNumbers)
        {
            var result = await _oracleRepository.GetTrackingNumberStatusAsync(trackingNumbersNumbers);
            return result;
        }


        public async Task<List<FileErrorStatus>> GetIftminFileErrorsAync()
        {
           
            var errorList = await _oracleRepository.GetIftminFileErrorsAync();
            return errorList;

        }


        public async Task<List<FileErrorStatus>> GetItmattFileErrorsAync()
        {
            
            var errorList = await _oracleRepository.GetItmattFileErrorsAync();
            return errorList;
        }

    }
}
