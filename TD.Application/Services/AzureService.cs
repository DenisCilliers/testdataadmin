﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IFTMIN.Engine.Models;
using Microsoft.Azure.Cosmos.Table;
using Microsoft.Extensions.Logging;
using TD.Application.Dto;
using TD.Application.Extensions;
using TD.Application.Map;
using TD.Domain.Entities.Model.TestData;
using TD.Infrastructure.Data.Azure.Repository;

namespace TD.Application.Services
{
    public interface IAzureService
    {
        Task<List<VendorDto>> GetAllVendorsAsync();
        Task<List<CustomerDto>> GetAllCustomersAsync();
        Task<List<ItemDto>> GetAllItemsAsync();
        Task<List<TestProfileDto>> GetAllTestProfilesAsync();

        Task<TestProfileDto> GetTestProfileByNameAsync(string profileName);
        Task<TestProfileDto> GetTestProfileAsync(string rowkey);

        Task<bool> SaveTestProfileAsync(TestProfileDto dto);
        Task<bool> SaveVendorAsync(VendorDto dto);
        Task<bool> SaveCustomerAsync(CustomerDto dto);
        Task<bool> SaveItemAsync(ItemDto dto);

        Task<VendorDto> GetVendorAsync(string rowkey);
        Task<CustomerDto> GetCustomerAsync(string rowkey);
        Task<CustomerDto> GetCustomerByNameAsync(string fullname);
        Task<ItemDto> GetItemAsync(string rowkey);
        Task<ItemDto> GetRandomItemAsync();

        Task<bool> DeleteTestProfileAsync(TestProfileDto dto);
        Task<bool> DeleteVendorAsync(VendorDto dto);
        Task<bool> DeleteCustomerAsync(CustomerDto dto);
        Task<bool> DeleteItemAsync(ItemDto dto);
        Task<bool> TestConnectionAsync();
    }
    
    public class AzureService : IAzureService
    {
        private readonly ILogger<AzureService> _logger;

        private readonly IVendorRepository _vendorRepository;
        private readonly ICustomerRepository _customerRepository;
        private readonly IItemRepository _itemRepository;
        private readonly ITestProfileRepository _testProfileRepository;

        public AzureService(IVendorRepository vendorRepository,
             ICustomerRepository customerRepository, 
             IItemRepository itemRepository, 
             ITestProfileRepository testProfileRepository,
            ILogger<AzureService> logger)
        {
            _logger = logger;
            _customerRepository = customerRepository;
            _itemRepository = itemRepository;
            _testProfileRepository = testProfileRepository;
            _vendorRepository = vendorRepository;
        }

      
        public async Task<List<VendorDto>> GetAllVendorsAsync()
        {
            var list = await _vendorRepository.GetAllAsync();
            
            var activeList = list
                .Where(r => r.Active)
                .OrderBy(r => r.Name)
                .Select(item => new VendorDto(item))
                .ToList();

            return activeList;
        }

        public async Task<List<CustomerDto>> GetAllCustomersAsync()
        {
            var list = await _customerRepository.GetAllAsync();

            var activeList = list
                .Where(r=>r.Active)
                .OrderBy(r => r.FullName)
                .Select(item => new CustomerDto(item))
                .ToList();

            return activeList;
        }

        public async Task<List<ItemDto>> GetAllItemsAsync()
        {
            var list = await _itemRepository.GetAllAsync();

            var activeList = list
                .Where(r => r.Active)
                .OrderBy(r => r.Description)
                .Select(item => new ItemDto(item))
                .ToList();
            
            return activeList;
        }

        public async Task<List<TestProfileDto>> GetAllTestProfilesAsync()
        {
            var list = await _testProfileRepository.GetAllAsync();
            
            var activeList = list
                .Where(r => r.Active)
                .OrderBy(r => r.ProfileName)
                .Select(item => new TestProfileDto(item))
                .ToList();
            
            return activeList;
        }
        
        public async Task<TestProfileDto> GetTestProfileByNameAsync(string profileName)
        {
            var query = new TableQuery<TestProfile>()
                .Where(TableQuery.GenerateFilterCondition("ProfileName", QueryComparisons.Equal, profileName));

            var entity = await _testProfileRepository.QueryAsync(query);

            //Get the Tracking numbers back as a string 
            var dto = entity.FirstOrDefault()?.ToDto();
            
            return dto;
        }

        public async Task<TestProfileDto> GetTestProfileAsync(string rowkey)
        {
            var entity = await _testProfileRepository.GetAsync(rowkey);

            return entity?.ToDto();
        }

        public async Task<bool> SaveTestProfileAsync(TestProfileDto dto)
        {
            //Check if Exists 
            var exists = await _testProfileRepository.GetAsync(dto.RowKey);
            
            if (exists != null)
            {
                _logger.LogDebug($"Update TestProfile: {exists.ProfileName}");

                //Map

                exists = dto.ToEntity(exists);
                
                //Save
                await _testProfileRepository.SaveAsync(exists);
            }
            else
            {
                _logger.LogDebug($"Insert TestProfile: {dto.ProfileName}");

                //New 
                var entity = dto.ToEntity();
                
                await _testProfileRepository.SaveAsync(entity);
            }

            return true;
        }

        public async Task<bool> SaveVendorAsync(VendorDto dto)
        {
            //Check if Exists 
            var exists =  await _vendorRepository.GetAsync(dto.RowKey);

            if (exists != null)
            {
                //Item Updated
                _logger.LogDebug($"Update Vendor: {dto.Name}");

                //Map
                exists = dto.ToEntity(exists);
                
                //Save
                await _vendorRepository.SaveAsync(exists);
            }
            else
            {
                _logger.LogDebug($"Insert Vendor: {dto.Name}");


                //New 
                var entity = dto.ToEntity();

                //New Item

                await _vendorRepository.SaveAsync(entity);
            }

            return true;
        }

        public async Task<bool> SaveCustomerAsync(CustomerDto dto)
        {
            //Check if Exists 
            var exists = await _customerRepository.GetAsync(dto.RowKey);

            if (exists != null)
            {
                //Item Updated
                _logger.LogDebug($"Update Customer: {dto.FullName}");

                //Map
                exists = dto.ToEntity(exists);
                
                //Save
                await _customerRepository.SaveAsync(exists);
            }
            else
            {
                _logger.LogDebug($"Insert Customer: {dto.FullName}");

                //New 
                var entity = dto.ToEntity();


                await _customerRepository.SaveAsync(entity);
            }

            return true;
        }

        public async Task<bool> SaveItemAsync(ItemDto dto)
        {
            //Check if Exists 
            var exists = await _itemRepository.GetAsync(dto.RowKey);

            if (exists != null)
            {
                //Item Updated
                _logger.LogDebug($"Update Item: {dto.Description}");

                //Map
                exists = dto.ToEntity(exists);

                //Save
                await _itemRepository.SaveAsync(exists);
            }
            else
            {
                _logger.LogDebug($"Insert Item: {dto.Description}");

                //New 
                var entity = dto.ToEntity();

                //Save
                await _itemRepository.SaveAsync(entity);
            }

            return true;
        }

        public async Task<VendorDto> GetVendorAsync(string rowkey)
        {
            var entity = await _vendorRepository.GetAsync(rowkey);
            
            return entity?.ToDto();
        }

        public async Task<CustomerDto> GetCustomerAsync(string rowkey)
        {
            var entity = await  _customerRepository.GetAsync(rowkey);

            return entity?.ToDto();

        }

        public async Task<CustomerDto> GetCustomerByNameAsync(string fullname)
        {
            var query = new TableQuery<Customer>()
                .Where(TableQuery.GenerateFilterCondition("FullName", QueryComparisons.Equal, fullname));

            //TableQuery<Vendor> query;
            var entity = await _customerRepository.QueryAsync(query);
            var dto = entity.FirstOrDefault().ToDto();

            return dto;
        }
       
        public async Task<ItemDto> GetItemAsync(string rowkey)
        {
            var entity = await _itemRepository.GetAsync(rowkey);

            return entity?.ToDto(); ;
        }

        public async Task<ItemDto> GetRandomItemAsync()
        {
            var items = await _itemRepository.GetAllAsync();

            var item = items.PickRandom().ToDto();

            return item;
        }

        public async Task<bool> DeleteTestProfileAsync(TestProfileDto dto)
        {
            var entity = await _testProfileRepository.GetAsync(dto.RowKey);

            await _testProfileRepository.DeleteAsync(entity);

            return true;
        }

        public async Task<bool> DeleteVendorAsync(VendorDto dto)
        {
            var entity = await _vendorRepository.GetAsync(dto.RowKey);

            await _vendorRepository.DeleteAsync(entity);
            return true;
        }

        public async Task<bool> DeleteCustomerAsync(CustomerDto dto)
        {
            var entity = await _customerRepository.GetAsync(dto.RowKey);

            await _customerRepository.DeleteAsync(entity);
            return true;
        }

        public async Task<bool> DeleteItemAsync(ItemDto dto)
        {
            var entity = await _itemRepository.GetAsync(dto.RowKey);

            await _itemRepository.DeleteAsync(entity);
            return true;
        }

        public async Task<bool> TestConnectionAsync()
        {
            var result = await GetAllTestProfilesAsync();

            return result != null;
        }
    }
}
