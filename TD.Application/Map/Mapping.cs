﻿using IFTMIN.Engine.Models;
using TD.Application.Dto;
using TD.Domain.Entities.Model.TestData;

namespace TD.Application.Map
{
    public static class Mapping
    {
        public  static TestProfile ToEntity(this TestProfileDto dto, TestProfile entity)
        {
            entity.Active = dto.Active;
            entity.ProfileName = dto.ProfileName;
            entity.VendorKey = dto.VendorKey;
            entity.CustomerKey = dto.CustomerKey;
            entity.ItemKey = dto.ItemKey;
            entity.FolderPath = dto.FolderPath;
            entity.TrackingNumbersJson = dto.TrackingNumbersJson;
            
            return entity;
        }

        public static TestProfile ToEntity(this TestProfileDto dto)
        {
            var entity = new TestProfile
            {
                Active = dto.Active,
                ProfileName = dto.ProfileName,
                VendorKey = dto.VendorKey,
                CustomerKey = dto.CustomerKey,
                FolderPath = dto.FolderPath,
                TrackingNumbersJson = dto.TrackingNumbersJson
            };

            entity.AssignRowKey();
            entity.AssignPartitionKey();

            return entity;
        }

        public static TestProfileDto ToDto(this TestProfile entity)
        {
            var dto = new TestProfileDto(entity);

            return dto;
        }

        public static Vendor ToEntity(this VendorDto dto)
        {
            var entity = new Vendor
            {
                Active = dto.Active,
                IFSAccount = dto.IFSAccount,
                Name = dto.Name,
                Type = dto.Type,
                Prefix = dto.Prefix,
                Postfix = dto.Postfix,
                StartNumber = dto.StartNumber,
                Min = dto.Min,
                Max = dto.Max,
                Email = dto.Email,
                Address1 = dto.Address1,
                Address2 = dto.Address2,
                Address3 = dto.Address3,
                Address4 = dto.Address4,
                City = dto.City,
                Country = dto.Country,
                PostCode = dto.PostCode,
                PhoneNumber = dto.PhoneNumber,
                VATIdentifier = dto.VATIdentifier,
                Currency = dto.Currency
            };

            entity.AssignRowKey();
            entity.AssignPartitionKey();
            
            return entity;
        }

        public static Vendor ToEntity(this VendorDto dto, Vendor entity)
        {
            entity.Active = dto.Active;
            entity.IFSAccount = dto.IFSAccount;
            entity.Name = dto.Name;
            entity.Type = dto.Type;
            entity.Prefix = dto.Prefix;
            entity.Postfix = dto.Postfix;
            entity.StartNumber = dto.StartNumber;
            entity.Min = dto.Min;
            entity.Max = dto.Max;
            entity.Email = dto.Email;
            entity.Address1 = dto.Address1;
            entity.Address2 = dto.Address2;
            entity.Address3 = dto.Address3;
            entity.Address4 = dto.Address4;
            entity.City = dto.City;
            entity.Country = dto.Country;
            entity.PostCode = dto.PostCode;
            entity.PhoneNumber = dto.PhoneNumber;
            entity.VATIdentifier = dto.VATIdentifier;
            entity.Currency = dto.Currency;
     
            return entity;
        }

        public static VendorDto ToDto( this Vendor entity)
        {
            var dto = new VendorDto(entity);

            return dto;
        }
        
        public static Customer ToEntity(this CustomerDto dto)
        {
            var entity = new Customer
            {
                Active = dto.Active,
                FullName = dto.FullName,
                FirstName = dto.FirstName,
                LastName = dto.LastName,
                Address1 = dto.Address1,
                Address2 = dto.Address2,
                Address3 = dto.Address3,
                Address4 = dto.Address4,
                City = dto.City,
                PostalCode = dto.PostalCode,
                Country = dto.Country,
                Phone = dto.Phone,
                Email = dto.Email
            };


            entity.AssignRowKey();
            entity.AssignPartitionKey();

            return entity;
        }

        public static Customer ToEntity(this CustomerDto dto, Customer entity)
        {
            entity.Active = dto.Active;
            entity.FullName = dto.FullName;
            entity.FirstName = dto.FirstName;
            entity.LastName = dto.LastName;
            entity.Address1 = dto.Address1;
            entity.Address2 = dto.Address2;
            entity.Address3 = dto.Address3;
            entity.Address4 = dto.Address4;
            entity.City = dto.City;
            entity.PostalCode = dto.PostalCode;
            entity.Country = dto.Country;
            entity.Phone = dto.Phone;
            entity.Email = dto.Email;

            return entity;
        }

        public static CustomerDto ToDto(this Customer entity)
        {
            var dto = new CustomerDto(entity);

            return dto;
        }

        public static Item ToEntity(this ItemDto dto)
        {
            var entity = new Item
            {
                Active = dto.Active,
                Description = dto.Description,
                Quantity = dto.Quantity,
                Weight = dto.Weight,
                ItemTariff = dto.ItemTariff,
                CountryOfOrigin = dto.CountryOfOrigin,
                Currency = dto.Currency,
                Price = dto.Price,
                TypeOfGoods = dto.TypeOfGoods
            };

            return entity;
        }

        public static Item ToEntity(this ItemDto dto, Item entity)
        {

            entity.Active = dto.Active;
            entity.Description = dto.Description;
            entity.Quantity = dto.Quantity;
            entity.Weight = dto.Weight;
            entity.ItemTariff = dto.ItemTariff;
            entity.CountryOfOrigin = dto.CountryOfOrigin;
            entity.Currency = dto.Currency;
            entity.Price = dto.Price;
            entity.TypeOfGoods = dto.TypeOfGoods;
            
            return entity;
        }

        public static ItemDto ToDto(this Item entity)
        {
            var dto = new ItemDto(entity);

            return dto;
        }

       
    }
}
