﻿using IFTMIN.Engine.Models;

namespace TD.Application.Dto
{
    public class CustomerDto: BaseDto
    {
        public CustomerDto()
        {
            
        }
        public CustomerDto(Customer entity)
        {
            RowKey = entity.RowKey;
            Active = entity.Active;
            Timestamp = entity.Timestamp.UtcDateTime;
            
            FullName = entity.FullName;
            FirstName = entity.FirstName;
            LastName = entity.LastName;
            Address1 = entity.Address1;
            Address2 = entity.Address2;
            Address3 = entity.Address3;
            Address4 = entity.Address4;
            City = entity.City;
            PostalCode = entity.PostalCode;
            Country = entity.Country;
            Phone = entity.Phone;
            Email = entity.Email;
        }
        
        public string FullName { get; set; }
        
        public string FirstName { get; set; }
        
        public string LastName { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string Address3 { get; set; }

        public string Address4 { get; set; }

        public string City { get; set; }

        public string PostalCode { get; set; }

        public string Country { get; set; }

        public string Phone { get; set; }

        public string Email { get; set; }
    }
}
