﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TD.Application.Dto
{
    public class GenerateDto
    {
        
        public string Prefix { get; set; }
        public string Postfix { get; set; }
        public double StartNumber { get; set; }
        public int NumberCount { get; set; }
        public string TrackingNumberFilePath { get; set; }
    }
}
