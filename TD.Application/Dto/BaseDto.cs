﻿using System;

namespace TD.Application.Dto
{
    public class BaseDto
    {
        public BaseDto()
        {
           
        }

        public string RowKey{ get; set; }

        public bool Active { get; set; }

        public DateTime Timestamp { get; set; }

       
    }
}
