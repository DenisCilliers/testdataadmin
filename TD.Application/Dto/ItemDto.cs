﻿using IFTMIN.Engine.Models;

namespace TD.Application.Dto
{
    public class ItemDto: BaseDto
    {
        public ItemDto()
        {
            
        }
        public ItemDto(Item entity)
        {
            RowKey = entity.RowKey;
            Active = entity.Active;
            Timestamp = entity.Timestamp.UtcDateTime;

            Description = entity.Description;
            Quantity = entity.Quantity;
            Weight = entity.Weight;
            ItemTariff = entity.ItemTariff;
            CountryOfOrigin = entity.CountryOfOrigin;
            Currency = entity.Currency;
            Price = entity.Price;
            TypeOfGoods = entity.TypeOfGoods;
        }

        public string Description { get; set; }

        public int Quantity { get; set; }

        public double Weight { get; set; }

        public string ItemTariff { get; set; }

        public string CountryOfOrigin { get; set; }

        public string Currency { get; set; }

        public double Price { get; set; }

        public string TypeOfGoods { get; set; }
        
    }
}
