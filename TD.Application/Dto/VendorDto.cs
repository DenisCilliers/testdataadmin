﻿using IFTMIN.Engine.Models;

namespace TD.Application.Dto
{
    public class VendorDto: BaseDto
    {
        public VendorDto()
        {
            
        }

        public VendorDto(Vendor entity)
        {
            RowKey = entity.RowKey;
            Active = entity.Active;
            Timestamp = entity.Timestamp.UtcDateTime;

            IFSAccount = entity.IFSAccount;
            Name = entity.Name;
            Type = entity.Type;
            Prefix = entity.Prefix;
            Postfix = entity.Postfix;
            StartNumber = entity.StartNumber;
            Min = entity.Min;
            Max = entity.Max;
            Email = entity.Email;
            Address1 = entity.Address1;
            Address2 = entity.Address2;
            Address3 = entity.Address3;
            Address4 = entity.Address4;
            City = entity.City;
            Country = entity.Country;
            PostCode = entity.PostCode;
            PhoneNumber = entity.PhoneNumber;
            VATIdentifier = entity.VATIdentifier;
            Currency = entity.Currency;
        }
        
        public string IFSAccount { get; set; }
        
        public string Name { get; set; }

        public string Type { get; set; }
        
        public string Prefix { get; set; }
        
        public string Postfix { get; set; }
        
        public double StartNumber { get; set; }
        
        public string Min { get; set; }
        
        public string Max { get; set; }

        public string Email { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string Address3 { get; set; }

        public string Address4 { get; set; }

        public string City { get; set; }

       
        public string Country { get; set; }

        public string PostCode { get; set; }

        public string PhoneNumber { get; set; }

        public string VATIdentifier { get; set; }
        
        public string Currency { get; set; }

        public bool Active { get; set; }

        public bool MyDelivery { get; set; }
    }
}
