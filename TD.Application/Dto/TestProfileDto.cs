﻿using Newtonsoft.Json;
using TD.Domain.Entities.Model.TestData;

namespace TD.Application.Dto
{
    public class TestProfileDto: BaseDto
    {
        public TestProfileDto()
        {
            
        }
        public TestProfileDto(TestProfile entity)
        {
            RowKey = entity.RowKey;
            Active = entity.Active;
            Timestamp = entity.Timestamp.UtcDateTime;

            ProfileName = entity.ProfileName;
            VendorKey = entity.VendorKey;
            CustomerKey = entity.CustomerKey;
            ItemKey = entity.ItemKey;
            FolderPath = entity.FolderPath;
            TrackingNumbersJson = entity.TrackingNumbersJson;
            BuildTrackingNumbers();
        }

        public string ProfileName { get; set; }
        
        public string VendorKey { get; set; }
      
        public string CustomerKey { get; set; }
        
        public string ItemKey { get; set; }
       
        public string FolderPath { get; set; }

        public string TrackingNumbersJson { get; set; }

        public TrackingNumbers TrackingNumbers { get; set; }

        private void BuildTrackingNumbers()
        {
            if (!string.IsNullOrWhiteSpace(TrackingNumbersJson))
                this.TrackingNumbers = JsonConvert.DeserializeObject<TrackingNumbers>(TrackingNumbersJson);
        }

    }
}
