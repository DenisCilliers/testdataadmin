﻿using System.Collections.Generic;

namespace TD.Application.Dto
{
    public class TrackingNumbers
    {
        public List<string> Numbers { get; set; }
    }
}
